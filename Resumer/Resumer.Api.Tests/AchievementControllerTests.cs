using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.Api.Controllers;
using Resumer.Models;
using Resumer.TestHelpers;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Api.Tests
{
    [TestClass]
    public class AchievementsTests
    {
        [TestMethod]
        public void AchievementController_Create_IsInvalidModel_Will400()
        {
            var controller = ControllerHelpers.GetAchievementController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Create(ModelHelpers.GetValidAchievement());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult)); 
        }
        
        [TestMethod]
        public void AchievementController_Create_IsValidModel_Will200()
        {
            var controller = ControllerHelpers.GetAchievementController();
            var result = controller.Create(ModelHelpers.GetValidAchievement());
            
            Assert.IsInstanceOfType(result, typeof(OkObjectResult)); 
        }
        
        [TestMethod]
        public void AchievementController_Update_IsInvalidModel_Will400()
        {
            var controller = ControllerHelpers.GetAchievementController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Update(0, ModelHelpers.GetValidAchievement());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult)); 
        }
        
        [TestMethod]
        public void AchievementController_Update_IsValidModel_Will200()
        {
            var controller = ControllerHelpers.GetAchievementController();
            var result = controller.Update(0, ModelHelpers.GetValidAchievement());
            
            Assert.IsInstanceOfType(result, typeof(OkObjectResult)); 
        }
    }
}