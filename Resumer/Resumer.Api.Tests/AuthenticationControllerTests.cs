using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Resumer.Api.Controllers;
using Resumer.Domain;
using Resumer.Domain.Interface;
using Resumer.Models;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Api.Tests
{
    [TestClass]
    public class AuthenticationTests
    {
        [TestMethod]
        public void AuthenticationController_Register_WithBadModel_Will400()
        {
            var controller = ControllerHelpers.GetAuthenticationController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Register(new AuthenticationModel());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }
        [TestMethod]
        public void AuthenticationController_Login_WithBadModel_Will400()
        {
            var controller = ControllerHelpers.GetAuthenticationController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Login(new AuthenticationModel());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }
        [TestMethod]
        public void AuthenticationController_ForgottenPassword_WithBadModel_Will400()
        {
            var controller = ControllerHelpers.GetAuthenticationController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.ForgottenPassword("test");
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }
        
        [TestMethod]
        public void AuthenticationController_Register_WithValidModel_WillReturnUser()
        {
            Mock<IAuthService> service = new Mock<IAuthService>();
            service.Setup(i => i.RegisterNewUser(It.IsAny<AuthenticationModel>()))
                .Returns(true);

            var controller = new AuthenticationController(service.Object);
            var result = controller.Register(new AuthenticationModel());
            
            Assert.IsInstanceOfType(result, typeof(OkResult));
        }
    }
}