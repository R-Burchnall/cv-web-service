using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.Api.Controllers;
using Resumer.Models;
using Resumer.TestHelpers;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Api.Tests
{
    [TestClass]
    public class InterestsTests
    {
        [TestMethod]
        public void InterestController_Create_IsInvalidModel_Will400()
        {
            var controller = ControllerHelpers.GetInterestController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Create(ModelHelpers.GetValidInterest());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult)); 
        }
        
        [TestMethod]
        public void InterestController_Create_IsValidModel_Will200()
        {
            var controller = ControllerHelpers.GetInterestController();
            var result = controller.Create(ModelHelpers.GetValidInterest());
            
            Assert.IsInstanceOfType(result, typeof(OkObjectResult)); 
        }
        
        [TestMethod]
        public void InterestController_Update_IsInvalidModel_Will400()
        {
            var controller = ControllerHelpers.GetInterestController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Update(0, ModelHelpers.GetValidInterest());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult)); 
        }
        
        [TestMethod]
        public void InterestController_Update_IsValidModel_Will200()
        {
            var controller = ControllerHelpers.GetInterestController();
            var result = controller.Update(0, ModelHelpers.GetValidInterest());
            
            Assert.IsInstanceOfType(result, typeof(OkObjectResult)); 
        }
    }
}