using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.Api.Controllers;
using Resumer.Models;
using Resumer.TestHelpers;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Api.Tests
{
    [TestClass]
    public class PersonalStatementTests
    {
        [TestMethod]
        public void PersonalStatementController_Create_IsInvalidModel_Will400()
        {
            var controller = ControllerHelpers.GetPersonalStatementController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Create(ModelHelpers.GetValidPersonalStatement());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult)); 
        }
        
        [TestMethod]
        public void PersonalStatementController_Create_IsValidModel_Will200()
        {
            var controller = ControllerHelpers.GetPersonalStatementController();
            var result = controller.Create(ModelHelpers.GetValidPersonalStatement());
            
            Assert.IsInstanceOfType(result, typeof(OkObjectResult)); 
        }
        
        [TestMethod]
        public void PersonalStatementController_Update_IsInvalidModel_Will400()
        {
            var controller = ControllerHelpers.GetPersonalStatementController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Update(0, ModelHelpers.GetValidPersonalStatement());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult)); 
        }
        
        [TestMethod]
        public void PersonalStatementController_Update_IsValidModel_Will200()
        {
            var controller = ControllerHelpers.GetPersonalStatementController();
            var result = controller.Update(0, ModelHelpers.GetValidPersonalStatement());
            
            Assert.IsInstanceOfType(result, typeof(OkObjectResult)); 
        }
    }
}