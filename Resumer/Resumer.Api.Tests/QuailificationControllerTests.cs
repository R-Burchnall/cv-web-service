using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.Api.Controllers;
using Resumer.Models;
using Resumer.TestHelpers;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Api.Tests
{
    [TestClass]
    public class QualificationsTests
    {
        [TestMethod]
        public void QualificationController_Create_IsInvalidModel_Will400()
        {
            var controller = ControllerHelpers.GetQualificationController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Create(ModelHelpers.GetValidQualification());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult)); 
        }
        
        [TestMethod]
        public void QualificationController_Create_IsValidModel_Will200()
        {
            var controller = ControllerHelpers.GetQualificationController();
            var result = controller.Create(ModelHelpers.GetValidQualification());
            
            Assert.IsInstanceOfType(result, typeof(OkObjectResult)); 
        }
        
        [TestMethod]
        public void QualificationController_Update_IsInvalidModel_Will400()
        {
            var controller = ControllerHelpers.GetQualificationController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Update(0, ModelHelpers.GetValidQualification());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult)); 
        }
        
        [TestMethod]
        public void QualificationController_Update_IsValidModel_Will200()
        {
            var controller = ControllerHelpers.GetQualificationController();
            var result = controller.Update(0, ModelHelpers.GetValidQualification());
            
            Assert.IsInstanceOfType(result, typeof(OkObjectResult)); 
        }
    }
}