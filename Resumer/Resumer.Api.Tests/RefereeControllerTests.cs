using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.Api.Controllers;
using Resumer.Models;
using Resumer.TestHelpers;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Api.Tests
{
    [TestClass]
    public class RefereesTests
    {
        [TestMethod]
        public void RefereeController_Create_IsInvalidModel_Will400()
        {
            var controller = ControllerHelpers.GetRefereeController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Create(ModelHelpers.GetValidReferee());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult)); 
        }
        
        [TestMethod]
        public void RefereeController_Create_IsValidModel_Will200()
        {
            var controller = ControllerHelpers.GetRefereeController();
            var result = controller.Create(ModelHelpers.GetValidReferee());
            
            Assert.IsInstanceOfType(result, typeof(OkObjectResult)); 
        }
        
        [TestMethod]
        public void RefereeController_Update_IsInvalidModel_Will400()
        {
            var controller = ControllerHelpers.GetRefereeController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Update(0, ModelHelpers.GetValidReferee());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult)); 
        }
        
        [TestMethod]
        public void RefereeController_Update_IsValidModel_Will200()
        {
            var controller = ControllerHelpers.GetRefereeController();
            var result = controller.Update(0, ModelHelpers.GetValidReferee());
            
            Assert.IsInstanceOfType(result, typeof(OkObjectResult)); 
        }
    }
}