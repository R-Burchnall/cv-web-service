using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.Api.Controllers;
using Resumer.Models;
using Resumer.TestHelpers;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Api.Tests
{
    [TestClass]
    public class SkillsTests
    {
        [TestMethod]
        public void SkillController_Create_IsInvalidModel_Will400()
        {
            var controller = ControllerHelpers.GetSkillController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Create(ModelHelpers.GetValidSkill());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult)); 
        }
        
        [TestMethod]
        public void SkillController_Create_IsValidModel_Will200()
        {
            var controller = ControllerHelpers.GetSkillController();
            var result = controller.Create(ModelHelpers.GetValidSkill());
            
            Assert.IsInstanceOfType(result, typeof(OkObjectResult)); 
        }
        
        [TestMethod]
        public void SkillController_Update_IsInvalidModel_Will400()
        {
            var controller = ControllerHelpers.GetSkillController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Update(0, ModelHelpers.GetValidSkill());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult)); 
        }
        
        [TestMethod]
        public void SkillController_Update_IsValidModel_Will200()
        {
            var controller = ControllerHelpers.GetSkillController();
            var result = controller.Update(0, ModelHelpers.GetValidSkill());
            
            Assert.IsInstanceOfType(result, typeof(OkObjectResult)); 
        }
    }
}