using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.Api.Controllers;
using Resumer.Models;
using Resumer.TestHelpers;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Api.Tests
{
    [TestClass]
    public class WorkHistorysTests
    {
        [TestMethod]
        public void WorkHistoryController_Create_IsInvalidModel_Will400()
        {
            var controller = ControllerHelpers.GetWorkHistoryController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Create(ModelHelpers.GetValidWorkHistory());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult)); 
        }
        
        [TestMethod]
        public void WorkHistoryController_Create_IsValidModel_Will200()
        {
            var controller = ControllerHelpers.GetWorkHistoryController();
            var result = controller.Create(ModelHelpers.GetValidWorkHistory());
            
            Assert.IsInstanceOfType(result, typeof(OkObjectResult)); 
        }
        
        [TestMethod]
        public void WorkHistoryController_Update_IsInvalidModel_Will400()
        {
            var controller = ControllerHelpers.GetWorkHistoryController();
            controller.ModelState.AddModelError("test", "test");
            var result = controller.Update(0, ModelHelpers.GetValidWorkHistory());
            
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult)); 
        }
        
        [TestMethod]
        public void WorkHistoryController_Update_IsValidModel_Will200()
        {
            var controller = ControllerHelpers.GetWorkHistoryController();
            var result = controller.Update(0, ModelHelpers.GetValidWorkHistory());
            
            Assert.IsInstanceOfType(result, typeof(OkObjectResult)); 
        }
    }
}