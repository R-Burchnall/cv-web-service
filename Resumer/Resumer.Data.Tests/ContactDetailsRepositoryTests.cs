using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.Data.Exceptions;
using Resumer.Models;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Data.Tests
{
    [TestClass]
    public class ContactDetailsRepositoryTests: RepositoryBaseTests
    {
        private ContactDetailsRepository _repo;
        private User _testUser;

        [TestInitialize]
        public void Setup()
        {
            using var context = new ServiceContext(_options);
            context.Database.EnsureCreated();
            
            _repo = new ContactDetailsRepository(_options);
            _testUser = ModelHelpers.GetValidUser();
            var seed = ModelHelpers.GetValidContactDetails();
            seed.User = null;
            seed.UserId = 0;
            _testUser.ContactDetails = seed;
            context.Users.Add(_testUser);
            var rows = context.SaveChanges();
            if (rows == 0)
            {
                throw new FailedToInsertException();
            }
        }

        [TestCleanup]
        public void Cleanup()
        {
            using var context = new ServiceContext(_options);
            context.Database.EnsureDeleted();
        }

        [TestMethod]
        public void ContactDetailsRepository_Read_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidContactDetails();
            testModel.UserId = _testUser.Id;
            var setModel = _repo.Create(testModel);
            var model = _repo.Read(setModel.Id);
            
            Assert.AreEqual(setModel.FirstName, model.FirstName);
            Assert.AreEqual(setModel.City, model.City);
            Assert.AreEqual(setModel.UserId, model.UserId);
        }

        [TestMethod]
        public void ContactDetailsRepository_Update_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidContactDetails();
            testModel.FirstName = "AAA";
            Assert.AreEqual("AAA", testModel.FirstName);
            Assert.AreEqual(0, testModel.Id);

            var setModel = _repo.Create(testModel);
            Assert.AreEqual("AAA", setModel.FirstName);
            Assert.AreNotEqual(0, setModel.Id);

            setModel.FirstName = "BBB";
            Assert.AreEqual("BBB", setModel.FirstName);
            var updatedModel = _repo.Update(setModel.Id, setModel);
            Assert.AreEqual("BBB", updatedModel.FirstName);
            Assert.AreEqual(setModel.Id, updatedModel.Id);
        }

        [TestMethod]
        public void ContactDetailsRepository_Create_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidContactDetails();
            Assert.AreEqual(0, testModel.Id);
            var model = _repo.Create(testModel);
            Assert.AreNotEqual(0, model.Id);
        }

        [TestMethod]
        public void ContactDetailsRepository_Delete_ReturnsTrue()
        {
            var success = _repo.Delete(_testUser.ContactDetails.Id, _testUser.Id);
            Assert.IsTrue(success);
        }
    }
}