using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.Models;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Data.Tests
{
    [TestClass]
    public class InterestRepositoryTests: RepositoryBaseTests

    {
    private InterestRepository _repo;
    private readonly List<Interest> _seedData = new List<Interest>();
    private User _testUser;

    [TestInitialize]
    public void Setup()
    {
        var userRepo = new UserRepository(_options);
        _testUser = userRepo.Create(ModelHelpers.GetValidUser());
        _repo = new InterestRepository(_options);

        var seed = ModelHelpers.GetValidInterest();
        seed.User = null;
        seed.UserId = _testUser.Id;
        _seedData.Add(_repo.Create(seed));

        seed = ModelHelpers.GetValidInterest();
        seed.User = null;
        seed.UserId = _testUser.Id;
        _seedData.Add(_repo.Create(seed));

        seed = ModelHelpers.GetValidInterest();
        seed.User = null;
        seed.UserId = _testUser.Id;
        _seedData.Add(_repo.Create(seed));
    }

    [TestCleanup]
    public void Cleanup()
    {
        using var context = new ServiceContext(_options);
        context.Interests.RemoveRange(_seedData);
    }


    [TestMethod]
    public void InterestRepository_List_ReturnsData()
    {
        var list = _repo.List(_testUser.Id);
        Assert.AreEqual(3, list.Count());
    }

    [TestMethod]
    public void InterestRepository_Read_ReturnsData()
    {
        var testModel = ModelHelpers.GetValidInterest();
        testModel.UserId = _testUser.Id;
        var setModel = _repo.Create(testModel);
        var model = _repo.Read(setModel.Id);

        Assert.AreEqual(setModel.Description, model.Description);
        Assert.AreEqual(setModel.Name, model.Name);
        Assert.AreEqual(setModel.UserId, model.UserId);
    }

    [TestMethod]
    public void InterestRepository_Update_ReturnsData()
    {
        var testModel = ModelHelpers.GetValidInterest();
        testModel.Name = "AAA";
        Assert.AreEqual("AAA", testModel.Name);
        Assert.AreEqual(0, testModel.Id);

        var setModel = _repo.Create(testModel);
        Assert.AreEqual("AAA", setModel.Name);
        Assert.AreNotEqual(0, setModel.Id);

        setModel.Name = "BBB";
        Assert.AreEqual("BBB", setModel.Name);
        var updatedModel = _repo.Update(setModel.Id, setModel);
        Assert.AreEqual("BBB", updatedModel.Name);
        Assert.AreEqual(setModel.Id, updatedModel.Id);
    }

    [TestMethod]
    public void InterestRepository_Create_ReturnsData()
    {
        var testModel = ModelHelpers.GetValidInterest();
        Assert.AreEqual(0, testModel.Id);
        var model = _repo.Create(testModel);
        Assert.AreNotEqual(0, model.Id);
    }

    [TestMethod]
    public void InterestRepository_Delete_ReturnsTrue()
    {
        var success = _repo.Delete(_seedData[0].Id, _testUser.Id);
        Assert.IsTrue(success);
    }
    }
}