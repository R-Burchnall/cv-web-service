using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.Data.Exceptions;
using Resumer.Models;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Data.Tests
{
    [TestClass]
    public class LoginAttemptRepositoryTests : RepositoryBaseTests
    {
        private LoginAttemptRepository _repo;
        private User _testUser;

        [TestInitialize]
        public void Setup()
        {
            using var context = new ServiceContext(_options);
            context.Database.EnsureCreated();
            
            _repo = new LoginAttemptRepository(_options);
            _testUser = ModelHelpers.GetValidUser();
            _testUser.LoginAttempts = new List<LoginAttempt>();
            
            var seed = ModelHelpers.GetValidLoginAttempt();
            seed.User = null;
            seed.UserId = _testUser.Id;
            _testUser.LoginAttempts.Add(seed);

            seed = ModelHelpers.GetValidLoginAttempt();
            seed.User = null;
            seed.UserId = _testUser.Id;
            _testUser.LoginAttempts.Add(seed);

            seed = ModelHelpers.GetValidLoginAttempt();
            seed.User = null;
            seed.UserId = _testUser.Id;
            _testUser.LoginAttempts.Add(seed);

            context.Users.Add(_testUser);
            var rows = context.SaveChanges();
            if (rows == 0)
            {
                throw new FailedToInsertException();
            }
        }

        [TestCleanup]
        public void Cleanup()
        {
            using var context = new ServiceContext(_options);
            context.Database.EnsureDeleted();
        }


        [TestMethod]
        public void LoginAttemptRepository_List_ReturnsData()
        {
            var list = _repo.List(_testUser.Id);
            Assert.AreEqual(3, list.Count());
        }

        [TestMethod]
        public void LoginAttemptRepository_Read_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidLoginAttempt();
            testModel.User = null;
            testModel.UserId = _testUser.Id;
            var setModel = _repo.Create(testModel);
            var model = _repo.Read(setModel.Id);

            Assert.AreEqual(setModel.IpAddress, model.IpAddress);
            Assert.AreEqual(setModel.AccessTime, model.AccessTime);
            Assert.AreEqual(setModel.UserId, model.UserId);
        }

        [TestMethod]
        public void LoginAttemptRepository_Update_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidLoginAttempt();
            var newIp = StringHelpers.RandomIP();
            testModel.IpAddress = newIp;
            Assert.AreEqual(newIp, testModel.IpAddress);
            Assert.AreEqual(0, testModel.Id);

            var setModel = _repo.Create(testModel);
            Assert.AreEqual(newIp, setModel.IpAddress);
            Assert.AreNotEqual(0, setModel.Id);

            newIp = StringHelpers.RandomIP();
            setModel.IpAddress = newIp;
            Assert.AreEqual(newIp, setModel.IpAddress);
            var updatedModel = _repo.Update(setModel.Id, setModel);
            Assert.AreEqual(newIp, updatedModel.IpAddress);
            Assert.AreEqual(setModel.Id, updatedModel.Id);
        }

        [TestMethod]
        public void LoginAttemptRepository_Create_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidLoginAttempt();
            Assert.AreEqual(0, testModel.Id);
            var model = _repo.Create(testModel);
            Assert.AreNotEqual(0, model.Id);
        }

        [TestMethod]
        public void LoginAttemptRepository_Delete_ReturnsTrue()
        {
            var success = _repo.Delete(_testUser.LoginAttempts[0].Id, _testUser.Id);
            Assert.IsTrue(success);
        }
    }
}