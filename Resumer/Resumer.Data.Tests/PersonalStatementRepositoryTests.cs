using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.Models;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Data.Tests
{
    [TestClass]
    public class PersonalStatementRepositoryTests: RepositoryBaseTests
    {
        private PersonalStatementRepository _repo;
        private readonly List<PersonalStatement> _seedData = new List<PersonalStatement>();
        private User _testUser;

        [TestInitialize]
        public void Setup()
        {
            var userRepo = new UserRepository(_options);
            _testUser = userRepo.Create(ModelHelpers.GetValidUser());
            _repo = new PersonalStatementRepository(_options);
            
            var seed = ModelHelpers.GetValidPersonalStatement();
            seed.User = null;
            seed.UserId = _testUser.Id;
            _seedData.Add(_repo.Create(seed));
            
            seed = ModelHelpers.GetValidPersonalStatement();
            seed.User = null;
            seed.UserId = _testUser.Id;
            _seedData.Add(_repo.Create(seed));
            
            seed = ModelHelpers.GetValidPersonalStatement();
            seed.User = null;
            seed.UserId = _testUser.Id;
            _seedData.Add(_repo.Create(seed));
        }

        [TestCleanup]
        public void Cleanup()
        {
            using var context = new ServiceContext(_options);
            context.PersonalStatements.RemoveRange(_seedData);
        }


        [TestMethod]
        public void PersonalStatementRepository_List_ReturnsData()
        {
            var list = _repo.List(_testUser.Id);
            Assert.AreEqual(3, list.Count());
        }

        [TestMethod]
        public void PersonalStatementRepository_Read_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidPersonalStatement();
            testModel.UserId = _testUser.Id;
            var setModel = _repo.Create(testModel);
            var model = _repo.Read(setModel.Id);
            
            Assert.AreEqual(setModel.Content, model.Content);
            Assert.AreEqual(setModel.UserId, model.UserId);
        }

        [TestMethod]
        public void PersonalStatementRepository_Update_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidPersonalStatement();
            testModel.Content = "AAA";
            Assert.AreEqual("AAA", testModel.Content);
            Assert.AreEqual(0, testModel.Id);

            var setModel = _repo.Create(testModel);
            Assert.AreEqual("AAA", setModel.Content);
            Assert.AreNotEqual(0, setModel.Id);

            setModel.Content = "BBB";
            Assert.AreEqual("BBB", setModel.Content);
            var updatedModel = _repo.Update(setModel.Id, setModel);
            Assert.AreEqual("BBB", updatedModel.Content);
            Assert.AreEqual(setModel.Id, updatedModel.Id);
        }

        [TestMethod]
        public void PersonalStatementRepository_Create_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidPersonalStatement();
            Assert.AreEqual(0, testModel.Id);
            var model = _repo.Create(testModel);
            Assert.AreNotEqual(0, model.Id);
        }

        [TestMethod]
        public void PersonalStatementRepository_Delete_ReturnsTrue()
        {
            var success = _repo.Delete(_seedData[0].Id, _testUser.Id);
            Assert.IsTrue(success);
        }
    }
}