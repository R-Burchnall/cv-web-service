using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.Models;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Data.Tests
{
    [TestClass]
    public class QualificationRepositoryTests: RepositoryBaseTests
    {
        private QualificationRepository _repo;
        private readonly List<Qualification> _seedData = new List<Qualification>();
        private User _testUser;

        [TestInitialize]
        public void Setup()
        {
            var userRepo = new UserRepository(_options);
            _testUser = userRepo.Create(ModelHelpers.GetValidUser());
            _repo = new QualificationRepository(_options);
            
            var seed = ModelHelpers.GetValidQualification();
            seed.User = null;
            seed.UserId = _testUser.Id;
            _seedData.Add(_repo.Create(seed));
            
            seed = ModelHelpers.GetValidQualification();
            seed.User = null;
            seed.UserId = _testUser.Id;
            _seedData.Add(_repo.Create(seed));
            
            seed = ModelHelpers.GetValidQualification();
            seed.User = null;
            seed.UserId = _testUser.Id;
            _seedData.Add(_repo.Create(seed));
        }

        [TestCleanup]
        public void Cleanup()
        {
            using var context = new ServiceContext(_options);
            context.Qualifications.RemoveRange(_seedData);
        }


        [TestMethod]
        public void QualificationRepository_List_ReturnsData()
        {
            var list = _repo.List(_testUser.Id);
            Assert.AreEqual(3, list.Count());
        }

        [TestMethod]
        public void QualificationRepository_Read_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidQualification();
            testModel.UserId = _testUser.Id;
            var setModel = _repo.Create(testModel);
            var model = _repo.Read(setModel.Id);
            
            Assert.AreEqual(setModel.Grade, model.Grade);
            Assert.AreEqual(setModel.Name, model.Name);
            Assert.AreEqual(setModel.UserId, model.UserId);
        }

        [TestMethod]
        public void QualificationRepository_Update_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidQualification();
            testModel.Name = "AAA";
            Assert.AreEqual("AAA", testModel.Name);
            Assert.AreEqual(0, testModel.Id);

            var setModel = _repo.Create(testModel);
            Assert.AreEqual("AAA", setModel.Name);
            Assert.AreNotEqual(0, setModel.Id);

            setModel.Name = "BBB";
            Assert.AreEqual("BBB", setModel.Name);
            var updatedModel = _repo.Update(setModel.Id, setModel);
            Assert.AreEqual("BBB", updatedModel.Name);
            Assert.AreEqual(setModel.Id, updatedModel.Id);
        }

        [TestMethod]
        public void QualificationRepository_Create_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidQualification();
            Assert.AreEqual(0, testModel.Id);
            var model = _repo.Create(testModel);
            Assert.AreNotEqual(0, model.Id);
        }

        [TestMethod]
        public void QualificationRepository_Delete_ReturnsTrue()
        {
            var success = _repo.Delete(_seedData[0].Id, _testUser.Id);
            Assert.IsTrue(success);
        }
    }
}