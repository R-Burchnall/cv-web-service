using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.Models;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Data.Tests
{
    [TestClass]
    public class RefereeRepositoryTests: RepositoryBaseTests
    {
        private RefereeRepository _repo;
        private readonly List<Referee> _seedData = new List<Referee>();
        private User _testUser;

        [TestInitialize]
        public void Setup()
        {
            var userRepo = new UserRepository(_options);
            _testUser = userRepo.Create(ModelHelpers.GetValidUser());
            _repo = new RefereeRepository(_options);
            
            var seed = ModelHelpers.GetValidReferee();
            seed.User = null;
            seed.UserId = _testUser.Id;
            _seedData.Add(_repo.Create(seed));
            
            seed = ModelHelpers.GetValidReferee();
            seed.User = null;
            seed.UserId = _testUser.Id;
            _seedData.Add(_repo.Create(seed));
            
            seed = ModelHelpers.GetValidReferee();
            seed.User = null;
            seed.UserId = _testUser.Id;
            _seedData.Add(_repo.Create(seed));
        }

        [TestCleanup]
        public void Cleanup()
        {
            using var context = new ServiceContext(_options);
            context.Referees.RemoveRange(_seedData);
        }


        [TestMethod]
        public void RefereeRepository_List_ReturnsData()
        {
            var list = _repo.List(_testUser.Id);
            Assert.AreEqual(3, list.Count());
        }

        [TestMethod]
        public void RefereeRepository_Read_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidReferee();
            testModel.UserId = _testUser.Id;
            var setModel = _repo.Create(testModel);
            var model = _repo.Read(setModel.Id);
            
            Assert.AreEqual(setModel.Name, model.Name);
            Assert.AreEqual(setModel.Relationship, model.Relationship);
            Assert.AreEqual(setModel.UserId, model.UserId);
        }

        [TestMethod]
        public void RefereeRepository_Update_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidReferee();
            testModel.Relationship = "AAA";
            Assert.AreEqual("AAA", testModel.Relationship);
            Assert.AreEqual(0, testModel.Id);

            var setModel = _repo.Create(testModel);
            Assert.AreEqual("AAA", setModel.Relationship);
            Assert.AreNotEqual(0, setModel.Id);

            setModel.Relationship = "BBB";
            Assert.AreEqual("BBB", setModel.Relationship);
            var updatedModel = _repo.Update(setModel.Id, setModel);
            Assert.AreEqual("BBB", updatedModel.Relationship);
            Assert.AreEqual(setModel.Id, updatedModel.Id);
        }

        [TestMethod]
        public void RefereeRepository_Create_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidReferee();
            Assert.AreEqual(0, testModel.Id);
            var model = _repo.Create(testModel);
            Assert.AreNotEqual(0, model.Id);
        }

        [TestMethod]
        public void RefereeRepository_Delete_ReturnsTrue()
        {
            var success = _repo.Delete(_seedData[0].Id, _testUser.Id);
            Assert.IsTrue(success);
        }
    }
}