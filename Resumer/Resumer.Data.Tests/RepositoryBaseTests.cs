﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace Resumer.Data.Tests
{
    public class RepositoryBaseTests
    {
        private SqliteConnection _connection;
        protected DbContextOptions<ServiceContext> _options { get; }

        protected RepositoryBaseTests()
        {
            _connection = new SqliteConnection("Data Source=../CVWebservice.test.sqlite");
            _options = new DbContextOptionsBuilder<ServiceContext>()
                .UseSqlite(_connection).Options;
    
            Seed();
        }

        private void Seed()
        {
            using (var context = new ServiceContext(_options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
            }
        }
    }
}