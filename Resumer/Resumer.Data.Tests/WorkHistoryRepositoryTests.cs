using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.Data.Exceptions;
using Resumer.Models;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Data.Tests
{
    [TestClass]
    public class WorkHistoryTests : RepositoryBaseTests
    {
        private WorkHistoryRepository _repo;
        private readonly List<WorkHistory> _seedData = new List<WorkHistory>();
        private User _testUser;


        [TestInitialize]
        public void Setup()
        {
            _testUser = ModelHelpers.GetValidUser();
            _testUser.WorkHistory = new List<WorkHistory>();
            _repo = new WorkHistoryRepository(_options);

            var seed = ModelHelpers.GetValidWorkHistory();
            seed.UserId = _testUser.Id;
            seed.User = null;
            _testUser.WorkHistory.Add(seed);

            seed = ModelHelpers.GetValidWorkHistory();
            seed.UserId = _testUser.Id;
            seed.User = null;
            _testUser.WorkHistory.Add(seed);

            seed = ModelHelpers.GetValidWorkHistory();
            seed.UserId = _testUser.Id;
            seed.User = null;
            _testUser.WorkHistory.Add(seed);

            using var context = new ServiceContext(_options);
            context.Users.Add(_testUser);
            var rows = context.SaveChanges();
            if (rows == 0)
            {
                throw new FailedToInsertException();
            }
        }

        [TestCleanup]
        public void Cleanup()
        {
            using var context = new ServiceContext(_options);
            context.WorkHistories.RemoveRange(_seedData);
        }


        [TestMethod]
        public void WorkHistoryRepository_List_ReturnsData()
        {
            var list = _repo.List(_testUser.Id);
            Assert.AreEqual(3, list.Count());
        }

        [TestMethod]
        public void WorkHistoryRepository_Read_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidWorkHistory();
            testModel.UserId = _testUser.Id;
            var setModel = _repo.Create(testModel);
            var model = _repo.Read(setModel.Id);

            Assert.AreEqual(setModel.JobTitle, model.JobTitle);
            Assert.AreEqual(setModel.OrganizationName, model.OrganizationName);
            Assert.AreEqual(setModel.UserId, model.UserId);
        }

        [TestMethod]
        public void WorkHistoryRepository_Update_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidWorkHistory();
            testModel.JobTitle = "AAA";
            Assert.AreEqual("AAA", testModel.JobTitle);
            Assert.AreEqual(0, testModel.Id);

            var setModel = _repo.Create(testModel);
            Assert.AreEqual("AAA", setModel.JobTitle);
            Assert.AreNotEqual(0, setModel.Id);

            setModel.JobTitle = "BBB";
            Assert.AreEqual("BBB", setModel.JobTitle);
            var updatedModel = _repo.Update(setModel.Id, setModel);
            Assert.AreEqual("BBB", updatedModel.JobTitle);
            Assert.AreEqual(setModel.Id, updatedModel.Id);
        }

        [TestMethod]
        public void WorkHistoryRepository_Create_ReturnsData()
        {
            var testModel = ModelHelpers.GetValidWorkHistory();
            Assert.AreEqual(0, testModel.Id);
            var model = _repo.Create(testModel);
            Assert.AreNotEqual(0, model.Id);
        }

        [TestMethod]
        public void WorkHistoryRepository_Delete_ReturnsTrue()
        {
            var success = _repo.Delete(_testUser.WorkHistory[0].Id, _testUser.Id);
            Assert.IsTrue(success);
        }
    }
}