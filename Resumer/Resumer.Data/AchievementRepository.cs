﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Resumer.Data.Exceptions;
using Resumer.Data.Interfaces;
using Resumer.Models;

namespace Resumer.Data
{
    public class AchievementRepository : RepositoryBase, IRepository<Achievement>
    {
        public AchievementRepository(DbContextOptions<ServiceContext> options = null) : base(options)
        {
        }

        public List<Achievement> List(int userId)
        {
            using var context = new ServiceContext(_options);
            return context.Achievements.Where(i => i.UserId == userId).ToList();
        }

        public Achievement Create(Achievement model)
        {
            using (var context = new ServiceContext(_options))
            {
                context.Achievements.Add(model);
                var rows = context.SaveChanges();
                if (rows > 0)
                {
                    return model;
                }

                throw new FailedToInsertException();
            }
        }

        public Achievement Read(int id)
        {
            using var context = new ServiceContext(_options);
            return context.Achievements.First(i => i.Id == id);
        }

        public Achievement Update(int id, Achievement model)
        {
            using var context = new ServiceContext(_options);
            context.Achievements.Update(model);     
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public bool Delete(int id, int ownerId)
        {
            using var context = new ServiceContext(_options);
            context.Achievements.Remove(context.Achievements.First(i => i.Id == id));     
            var rows = context.SaveChanges();
            return rows > 0;
        }
    }
}