﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Resumer.Data.Exceptions;
using Resumer.Data.Interfaces;
using Resumer.Models;

namespace Resumer.Data
{
    public class AchievementsRepository : RepositoryBase, IRepository<Achievement>
    {
        public AchievementsRepository(DbContextOptions<ServiceContext> options = null) : base(options)
        {
        }

        public List<Achievement> List(int userId)
        {
            using var context = new ServiceContext(_options);
            return context.Achievements.Where(i => i.User.Id == userId).ToList();
        }

        public Achievement Create(Achievement model)
        {
            using var context = new ServiceContext(_options);
            context.Achievements.Add(model);
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public Achievement Read(int id)
        {
            using var context = new ServiceContext(_options);
            return context.Achievements.First(i => i.Id == id);
        }

        public Achievement Update(int id, Achievement model)
        {
            using var context = new ServiceContext(_options);
            var old = context.Achievements.First(i => i.Id == id);
            context.Entry(old).CurrentValues.SetValues(model);
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public bool Delete(int id, int ownerId)
        {
            using (var context = new ServiceContext(_options))
            {
                context.Achievements.Remove(context.Achievements.First(i => i.Id == id && i.User.Id == ownerId));
                var rows = context.SaveChanges();
                return rows > 0;
            }
        }
    }
}