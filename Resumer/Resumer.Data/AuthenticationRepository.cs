﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Resumer.Models;

namespace Resumer.Data
{
    public class AuthenticationRepository: RepositoryBase
    {
        public AuthenticationRepository(DbContextOptions<ServiceContext> options = null) : base(options)
        {
        }

        public bool CreateNewUser(AuthenticationModel authenticationModel, out string confirmationCode,
            out List<string> errors)
        {
            using (var context = new ServiceContext(_options))
            {
                errors = new List<string>();
                var u = BuildUser(authenticationModel);

                context.Users.Add(u);
                var rows = context.SaveChanges();
                if (rows > 0)
                {
                    confirmationCode = u.ConfirmationCode;
                    return true;
                }

                errors.Add("Was not able to insert into the database.");
                confirmationCode = null;
                return false;
            }
        }

        private User BuildUser(AuthenticationModel authenticationModel)
        {
            User u = new User();
            u.EmailAddress = authenticationModel.EmailAddress;
            u.PasswordDigest = BCrypt.Net.BCrypt.HashPassword(authenticationModel.Password);
            u.ConfirmationCode = Guid.NewGuid().ToString();
            return u;
        }

        public bool UserWithEmailExists(string emailAddress)
        {
            using (var context = new ServiceContext(_options))
            {
                return context.Users.Any(i => i.EmailAddress == emailAddress);
            }
        }

        public bool AssignResetPasswordToken(string emailAddress, string token)
        {
            using (var context = new ServiceContext(_options))
            {
                var user = context.Users.First(i => i.EmailAddress == emailAddress);
                if (user == null)
                {
                    return false;
                }

                user.ForgottenPasswordToken = token;
                var rows = context.SaveChanges();
                if (rows > 0)
                {
                    return true;
                }

                return false;
            }
        }
    }
}