﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Resumer.Data.Exceptions;
using Resumer.Data.Interfaces;
using Resumer.Models;

namespace Resumer.Data
{
    public class ContactDetailsRepository : RepositoryBase, IRepository<ContactDetails>
    {
        public ContactDetailsRepository(DbContextOptions<ServiceContext> options = null) : base(options)
        {
        }

        public List<ContactDetails> List(int userId)
        {
            using var context = new ServiceContext(_options);
            return context.ContactDetails.Where(i => i.User.Id == userId).ToList();
        }

        public ContactDetails Create(ContactDetails model)
        {
            using var context = new ServiceContext(_options);
            context.ContactDetails.Add(model);
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public ContactDetails Read(int id)
        {
            using var context = new ServiceContext(_options);
            return context.ContactDetails.First(i => i.Id == id);
        }

        public ContactDetails Update(int id, ContactDetails model)
        {
            using var context = new ServiceContext(_options);
            context.ContactDetails.Update(model);
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public bool Delete(int id, int ownerId)
        {
            using var context = new ServiceContext(_options);
            context.ContactDetails.Remove(context.ContactDetails.First(i => i.Id == id));
            var rows = context.SaveChanges();
            return rows > 0;
        }
    }
}