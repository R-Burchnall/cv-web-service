﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Resumer.Data.Exceptions;
using Resumer.Data.Interfaces;
using Resumer.Models;

namespace Resumer.Data
{
    public class InterestRepository : RepositoryBase, IRepository<Interest>
    {
        public InterestRepository(DbContextOptions<ServiceContext> options = null) : base(options)
        {
        }

        public List<Interest> List(int userId)
        {
            using var context = new ServiceContext(_options);
            return context.Interests.Where(i => i.User.Id == userId).ToList();
        }

        public Interest Create(Interest model)
        {
            using var context = new ServiceContext(_options);
            context.Interests.Add(model);
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public Interest Read(int id)
        {
            using var context = new ServiceContext(_options);
            return context.Interests.First(i => i.Id == id);
        }

        public Interest Update(int id, Interest model)
        {
            using var context = new ServiceContext(_options);
            context.Interests.Update(model);     
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public bool Delete(int id, int ownerId)
        {
            using var context = new ServiceContext(_options);
            context.Interests.Remove(context.Interests.First(i => i.Id == id));
            var rows = context.SaveChanges();
            return rows > 0;
        }
    }
}