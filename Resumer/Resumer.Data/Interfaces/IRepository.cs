﻿using System.Collections;
using System.Collections.Generic;

namespace Resumer.Data.Interfaces
{
    public interface IRepository<T>
    {
        List<T> List(int userId);
        T Create(T model);
        T Read(int id);
        T Update(int id, T model);
        bool Delete(int id, int ownerId);
    }
}