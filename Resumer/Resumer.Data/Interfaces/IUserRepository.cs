﻿using System.Collections.Generic;
using Resumer.Models;

namespace Resumer.Data.Interfaces
{
    public interface IUserRepository
    {
        User AuthenticateUser(AuthenticationModel authenticationModel);

        string CreateNewUser(AuthenticationModel authenticationModel);

        IEnumerable<User> List();
        User Get(int userId);
    }
}