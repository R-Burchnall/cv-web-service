﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Resumer.Data.Exceptions;
using Resumer.Data.Interfaces;
using Resumer.Models;

namespace Resumer.Data
{
    public class LoginAttemptRepository : RepositoryBase, IRepository<LoginAttempt>
    {
        public LoginAttemptRepository(DbContextOptions<ServiceContext> options = null) : base(options)
        {
        }

        public List<LoginAttempt> List(int userId)
        {
            using var context = new ServiceContext(_options);
            return context.LoginAttempts.Where(i => i.User.Id == userId).ToList();
        }

        public LoginAttempt Create(LoginAttempt model)
        {
            using var context = new ServiceContext(_options);
            context.LoginAttempts.Add(model);
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public LoginAttempt Read(int id)
        {
            using var context = new ServiceContext(_options);
            return context.LoginAttempts.First(i => i.Id == id);
        }

        public LoginAttempt Update(int id, LoginAttempt model)
        {
            using var context = new ServiceContext(_options);
            context.LoginAttempts.Update(model);     
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public bool Delete(int id, int ownerId)
        {
            using var context = new ServiceContext(_options);
            context.LoginAttempts.Remove(context.LoginAttempts.First(i => i.Id == id));
            var rows = context.SaveChanges();
            return rows > 0;
        }
    }
}