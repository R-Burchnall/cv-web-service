﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Resumer.Data.Exceptions;
using Resumer.Data.Interfaces;
using Resumer.Models;

namespace Resumer.Data
{
    public class PersonalStatementRepository : RepositoryBase, IRepository<PersonalStatement>
    {
        public PersonalStatementRepository(DbContextOptions<ServiceContext> options = null) : base(options)
        {
        }

        public List<PersonalStatement> List(int userId)
        {
            using var context = new ServiceContext(_options);
            return context.PersonalStatements.Where(i => i.User.Id == userId).ToList();
        }

        public PersonalStatement Create(PersonalStatement model)
        {
            using var context = new ServiceContext(_options);
            context.PersonalStatements.Add(model);
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public PersonalStatement Read(int id)
        {
            using var context = new ServiceContext(_options);
            return context.PersonalStatements.First(i => i.Id == id);
        }

        public PersonalStatement Update(int id, PersonalStatement model)
        {
            using var context = new ServiceContext(_options);
            context.PersonalStatements.Update(model);
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public bool Delete(int id, int ownerId)
        {
            using var context = new ServiceContext(_options);
            context.PersonalStatements.Remove(context.PersonalStatements.First(i => i.Id == id));
            var rows = context.SaveChanges();
            return rows > 0;
        }
    }
}