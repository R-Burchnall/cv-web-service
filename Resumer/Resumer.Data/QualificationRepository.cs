﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Resumer.Data.Exceptions;
using Resumer.Data.Interfaces;
using Resumer.Models;

namespace Resumer.Data
{
    public class QualificationRepository : RepositoryBase, IRepository<Qualification>
    {
        public QualificationRepository(DbContextOptions<ServiceContext> options = null) : base(options)
        {
        }

        public List<Qualification> List(int userId)
        {
            using var context = new ServiceContext(_options);
            return context.Qualifications.Where(i => i.User.Id == userId).ToList();
        }

        public Qualification Create(Qualification model)
        {
            using var context = new ServiceContext(_options);
            context.Qualifications.Add(model);
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public Qualification Read(int id)
        {
            using var context = new ServiceContext(_options);
            return context.Qualifications.First(i => i.Id == id);
        }

        public Qualification Update(int id, Qualification model)
        {
            using var context = new ServiceContext(_options);
            context.Qualifications.Update(model);     
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public bool Delete(int id, int ownerId)
        {
            using var context = new ServiceContext(_options);
            context.Qualifications.Remove(context.Qualifications.First(i => i.Id == id));
            var rows = context.SaveChanges();
            return rows > 0;
        }
    }
}