﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Resumer.Data.Exceptions;
using Resumer.Data.Interfaces;
using Resumer.Models;

namespace Resumer.Data
{
    public class RefereeRepository : RepositoryBase, IRepository<Referee>
    {
        public RefereeRepository(DbContextOptions<ServiceContext> options = null) : base(options)
        {
        }

        public List<Referee> List(int userId)
        {
            using var context = new ServiceContext(_options);
            return context.Referees.Where(i => i.User.Id == userId).ToList();
        }

        public Referee Create(Referee model)
        {
            using var context = new ServiceContext(_options);
            context.Referees.Add(model);
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public Referee Read(int id)
        {
            using var context = new ServiceContext(_options);
            return context.Referees.First(i => i.Id == id);
        }

        public Referee Update(int id, Referee model)
        {
            using var context = new ServiceContext(_options);
            context.Referees.Update(model);     
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public bool Delete(int id, int ownerId)
        {
            using var context = new ServiceContext(_options);
            context.Referees.Remove(context.Referees.First(i => i.Id == id));
            var rows = context.SaveChanges();
            return rows > 0;
        }
    }
}