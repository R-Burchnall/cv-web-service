﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Resumer.Data
{
    public class RepositoryBase
    {
        protected DbContextOptions _options;

        public RepositoryBase(DbContextOptions<ServiceContext> options = null)
        {
            if (options != null)
            {
                _options = options;
            }
            else
            {
                _options = new DbContextOptionsBuilder()
                    .UseLoggerFactory(LoggerFactory.Create(builder => { builder.AddDebug(); }))
                    .UseSqlite("Data Source=../CVWebservice.sqlite").Options;
            }
        }
    }
}