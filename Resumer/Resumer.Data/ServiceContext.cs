using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Resumer.Models;

namespace Resumer.Data
{
    public class ServiceContext : DbContext
    { public ServiceContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<Achievement> Achievements { get; set; }
        public virtual DbSet<ContactDetails> ContactDetails { get; set; }
        public virtual DbSet<Interest> Interests { get; set; }
        public virtual DbSet<PersonalStatement> PersonalStatements { get; set; }
        public virtual DbSet<LoginAttempt> LoginAttempts { get; set; }
        public virtual DbSet<Qualification> Qualifications { get; set; }
        public virtual DbSet<Referee> Referees { get; set; }
        public virtual DbSet<Skill> Skills { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<WorkHistory> WorkHistories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>().HasIndex(i => i.EmailAddress).IsUnique();
        }
    }
}