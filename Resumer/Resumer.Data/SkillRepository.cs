﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Resumer.Data.Exceptions;
using Resumer.Data.Interfaces;
using Resumer.Models;

namespace Resumer.Data
{
    public class SkillRepository : RepositoryBase, IRepository<Skill>
    {
        public SkillRepository(DbContextOptions<ServiceContext> options = null) : base(options)
        {
        }

        public List<Skill> List(int userId)
        {
            using var context = new ServiceContext(_options);
            return context.Skills.Where(i => i.User.Id == userId).ToList();
        }

        public Skill Create(Skill model)
        {
            using var context = new ServiceContext(_options);
            context.Skills.Add(model);
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public Skill Read(int id)
        {
            using var context = new ServiceContext(_options);
            return context.Skills.First(i => i.Id == id);
        }

        public Skill Update(int id, Skill model)
        {
            using var context = new ServiceContext(_options);
            context.Skills.Update(model);     
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public bool Delete(int id, int ownerId)
        {
            using var context = new ServiceContext(_options);
            context.Skills.Remove(context.Skills.First(i => i.Id == id));
            var rows = context.SaveChanges();
            return rows > 0;
        }
    }
}