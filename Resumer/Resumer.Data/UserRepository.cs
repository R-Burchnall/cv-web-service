﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Resumer.Data.Exceptions;
using Resumer.Data.Interfaces;
using Resumer.Models;

namespace Resumer.Data
{
    public class UserRepository : RepositoryBase, IUserRepository
    {
        public UserRepository(DbContextOptions<ServiceContext> options = null) : base(options)
        {
        }

        public User Create(User getValidUser)
        {
            using var context = new ServiceContext(_options);
            context.Users.Add(getValidUser);
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return getValidUser;
            }

            throw new FailedToInsertException();
        }

        public User AuthenticateUser(AuthenticationModel authenticationModel)
        {
            using var context = new ServiceContext(_options);
            var user = context.Users
                .Include(i => i.Achievements)
                .Include(i => i.Interests)
                .Include(i => i.Qualifications)
                .Include(i => i.Referees)
                .Include(i => i.Skills)
                .Include(i => i.PersonalStatements)
                .Include(i => i.WorkHistory)
                .Include(i => i.ContactDetails)
                .First(i => i.EmailAddress == authenticationModel.EmailAddress);
            var verify = BCrypt.Net.BCrypt.Verify(authenticationModel.Password, user.PasswordDigest);
            if (verify)
            {
                return user;
            }

            return null;
        }

        public string CreateNewUser(AuthenticationModel authenticationModel)
        {
            using var context = new ServiceContext(_options);
            var confirmationCode = Guid.NewGuid().ToString();
            var newUser = new User()
            {
                EmailAddress = authenticationModel.EmailAddress,
                PasswordDigest = BCrypt.Net.BCrypt.HashPassword(authenticationModel.Password),
                ConfirmationCode = confirmationCode
            };

            context.Users.Add(newUser);
            var rows = context.SaveChanges();
            if (rows == 0)
            {
                throw new FailedToInsertException();
            }

            return confirmationCode;
        }

        public IEnumerable<User> List()
        {
            using var context = new ServiceContext(_options);
            return context.Users.ToList();
        }

        public User Get(int userId)
        {
            using var context = new ServiceContext(_options);
            return context.Users
                .Include(i => i.Achievements)
                .Include(i => i.Interests)
                .Include(i => i.Qualifications)
                .Include(i => i.Referees)
                .Include(i => i.Skills)
                .Include(i => i.PersonalStatements)
                .Include(i => i.WorkHistory)
                .Include(i => i.ContactDetails)
                .First(i => i.Id == userId);
        }
    }
}