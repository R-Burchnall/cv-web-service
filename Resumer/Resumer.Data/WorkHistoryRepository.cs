﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Resumer.Data.Exceptions;
using Resumer.Data.Interfaces;
using Resumer.Models;

namespace Resumer.Data
{
    public class WorkHistoryRepository : RepositoryBase, IRepository<WorkHistory>
    {
        public WorkHistoryRepository(DbContextOptions<ServiceContext> options = null) : base(options)
        {
        }

        public List<WorkHistory> List(int userId)
        {
            using var context = new ServiceContext(_options);
            return context.WorkHistories.Where(i => i.User.Id == userId).ToList();
        }

        public WorkHistory Create(WorkHistory model)
        {
            using var context = new ServiceContext(_options);
            context.WorkHistories.Add(model);
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public WorkHistory Read(int id)
        {
            using var context = new ServiceContext(_options);
            return context.WorkHistories.First(i => i.Id == id);
        }

        public WorkHistory Update(int id, WorkHistory model)
        {
            using var context = new ServiceContext(_options);
            context.WorkHistories.Update(model);     
            var rows = context.SaveChanges();
            if (rows > 0)
            {
                return model;
            }

            throw new FailedToInsertException();
        }

        public bool Delete(int id, int ownerId)
        {
            using var context = new ServiceContext(_options);
            context.WorkHistories.Remove(context.WorkHistories.First(i => i.Id == id));
            var rows = context.SaveChanges();
            return rows > 0;
        }
    }
}