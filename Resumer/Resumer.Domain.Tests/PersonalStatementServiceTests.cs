using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Resumer.Data;
using Resumer.Data.Interfaces;
using Resumer.Models;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Domain.Tests
{
    [TestClass]
    public class PersonalStatementServiceTests
    {
        [TestMethod]
        public void List_WithUser_ReturnsList()
        {
            Mock<IRepository<PersonalStatement>> repo = new Mock<IRepository<PersonalStatement>>();
            repo.Setup(i => i.List(It.IsAny<int>()))
                .Returns(new List<PersonalStatement>()
                {
                    ModelHelpers.GetValidPersonalStatement(),
                    ModelHelpers.GetValidPersonalStatement(),
                    ModelHelpers.GetValidPersonalStatement()
                });

            var service = new PersonalStatementService(repo.Object);
            var list = service.List(ModelHelpers.GetValidUser());

            repo.Verify(i => i.List(It.IsAny<int>()), Times.Once);
            Assert.IsNotNull(list);
            Assert.AreEqual(3, list.Count());
        }

        [TestMethod]
        public void Create_WithUserAndModel_ReturnsCreatedModel()
        {
            var test = ModelHelpers.GetValidPersonalStatement();
            var user = ModelHelpers.GetValidUser();
            user.Id = 10;
            test.User = user;
            Mock<IRepository<PersonalStatement>> repo = new Mock<IRepository<PersonalStatement>>();
            repo.Setup(i => i.Create(It.IsAny<PersonalStatement>()))
                .Returns((PersonalStatement input) =>
                {
                    input.Id = 1;
                    return input;
                });

            var service = new PersonalStatementService(repo.Object);
            Assert.AreEqual(0, test.Id);
            Assert.AreEqual(0, test.UserId);
            var model = service.Create(user, test);

            repo.Verify(i => i.Create(It.IsAny<PersonalStatement>()), Times.Once);
            Assert.IsNotNull(model);
            Assert.AreEqual(user.Id, test.UserId);
            Assert.AreNotEqual(0, model.Id);
        }

        [TestMethod]
        public void Update_WithUserAndModel_ReturnsModel()
        {
            var test = ModelHelpers.GetValidPersonalStatement();
            var user = ModelHelpers.GetValidUser();
            user.Id = 10;
            test.User = user;
            Mock<IRepository<PersonalStatement>> repo = new Mock<IRepository<PersonalStatement>>();
            repo.Setup(i => i.Update(It.IsAny<int>(), It.IsAny<PersonalStatement>()))
                .Returns((int i, PersonalStatement a) =>
                {
                    a.Id = i;
                    return a;
                });

            var service = new PersonalStatementService(repo.Object);
            Assert.AreEqual(0, test.Id);
            Assert.AreEqual(0, test.UserId);
            var model = service.Update(user, 1, test);

            repo.Verify(i => i.Update(It.IsAny<int>(), It.IsAny<PersonalStatement>()), Times.Once);
            Assert.IsNotNull(model);
            Assert.AreNotEqual(0, model.Id);
        }

        [TestMethod]
        public void Delete_WithUserAndModel_ReturnsTrue()
        {
            var test = ModelHelpers.GetValidPersonalStatement();
            var user = ModelHelpers.GetValidUser();
            user.Id = 10;
            test.User = user;
            Mock<IRepository<PersonalStatement>> repo = new Mock<IRepository<PersonalStatement>>();
            repo.Setup(i => i.Delete(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(true);

            var service = new PersonalStatementService(repo.Object);
            Assert.AreEqual(0, test.Id);
            Assert.AreEqual(0, test.UserId);
            var response = service.Delete(user, 1);

            repo.Verify(i => i.Delete(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.AreEqual(true, response);
        }
    }
}