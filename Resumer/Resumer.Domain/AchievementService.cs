﻿using System.Collections.Generic;
using Resumer.Data.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Domain
{
    public class AchievementService: IService<Achievement>
    {
        private readonly IRepository<Achievement> _repo;

        public AchievementService(IRepository<Achievement> repo)
        {
            _repo = repo;
        }

        public IEnumerable<Achievement> List(User user)
        {
            return _repo.List(user.Id);
        }

        public Achievement Create(User user, Achievement model)
        {
            if (model.User != null && model.UserId == 0)
            {
                model.UserId = model.User.Id;
                model.User = null;
            }
            return _repo.Create(model);
        }

        public Achievement Read(User user, int id)
        {
            return _repo.Read(user.Id);
        }

        public Achievement Update(User user, int id, Achievement model)
        {
            return _repo.Update(id, model);
        }

        public bool Delete(User user, int id)
        {
            return _repo.Delete(id,user.Id);
        }
    }
}