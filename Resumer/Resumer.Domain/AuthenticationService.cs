﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Resumer.Data;
using Resumer.Data.Interfaces;
using Resumer.Domain.Exceptions;
using Resumer.Domain.Interface;
using Resumer.Models;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace Resumer.Domain
{
    public class AuthenticationService : IAuthService
    {
        private readonly IConfiguration _config;
        private readonly IUserRepository _userRepo;

        public AuthenticationService(IConfiguration config,
            IUserRepository userRepo)
        {
            _config = config;
            _userRepo = userRepo;
        }

        public User AuthenticateUser(AuthenticationModel authenticationModel)
        {
            return _userRepo.AuthenticateUser(authenticationModel);
        }

        public User GetUserByID(int userId)
        {
            User u = _userRepo.Get(userId);
            u.PersonalStatements = u.PersonalStatements.OrderByDescending(i => i.CreatedAt).ToList();
            return u;
        }


        public bool RegisterNewUser(AuthenticationModel authenticationModel)
        {
            var confirmationCode = _userRepo.CreateNewUser(authenticationModel);
            if (confirmationCode != string.Empty)
            {
                DispatchConfirmationEmail(authenticationModel.EmailAddress, confirmationCode);
            }

            return true;
        }

        private void DispatchConfirmationEmail(string authenticationModelEmailAddress, string confirmationCode)
        {
            // Stub for sending an email via Amazon SES or other SMTP provider
        }

        private void DispatchResetPasswordEmail(string emailAddress, string token)
        {
            //Stub for sending an email via Amazon SES or other SMTP provider
        }

        public bool AttemptToSendPasswordReset(string emailAddress)
        {
            var repo = new AuthenticationRepository();
            var success = repo.UserWithEmailExists(emailAddress);
            if (!success)
            {
                throw new UserAlreadyExistsException();
            }

            var token = Guid.NewGuid().ToString();
            success = repo.AssignResetPasswordToken(emailAddress, token);
            if (!success)
            {
                throw new FailedToCreateResetToken();
            }

            DispatchResetPasswordEmail(emailAddress, token);
            return true;
        }

        public IEnumerable<User> ListUsers()
        {
            return _userRepo.List();
        }


        public string GenerateJwtToken(User user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:SecretKey"]));
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.EmailAddress),
                new Claim("userId", user.Id.ToString()),
                new Claim("emailAddress", user.EmailAddress),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Audience"],
                claims: claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: credentials
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}