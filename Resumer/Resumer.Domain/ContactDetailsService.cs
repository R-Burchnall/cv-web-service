﻿using System.Collections.Generic;
using Resumer.Data.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Domain
{
    public class ContactDetailsService: IService<ContactDetails>
    {
        private readonly IRepository<ContactDetails> _repo;

        public ContactDetailsService(IRepository<ContactDetails> repo)
        {
            _repo = repo;
        }

        public IEnumerable<ContactDetails> List(User user)
        {
            return _repo.List(user.Id);
        }

        public ContactDetails Create(User user, ContactDetails model)
        {
            if (model.User != null && model.UserId == 0)
            {
                model.UserId = model.User.Id;
                model.User = null;
            }
            return _repo.Create(model);
        }

        public ContactDetails Read(User user, int id)
        {
            return _repo.Read(user.Id);
        }

        public ContactDetails Update(User user, int id, ContactDetails model)
        {
            return _repo.Update(id, model);
        }

        public bool Delete(User user, int id)
        {
            return _repo.Delete(id,user.Id);
        }
    }
}