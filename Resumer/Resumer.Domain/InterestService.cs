﻿using System.Collections.Generic;
using Resumer.Data.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Domain
{
    public class InterestService: IService<Interest>
    {
        private readonly IRepository<Interest> _repo;

        public InterestService(IRepository<Interest> repo)
        {
            _repo = repo;
        }

        public IEnumerable<Interest> List(User user)
        {
            return _repo.List(user.Id);
        }

        public Interest Create(User user, Interest model)
        {
            if (model.User != null && model.UserId == 0)
            {
                model.UserId = model.User.Id;
                model.User = null;
            }
            return _repo.Create(model);
        }

        public Interest Read(User user, int id)
        {
            return _repo.Read(user.Id);
        }

        public Interest Update(User user, int id, Interest model)
        {
            return _repo.Update(id, model);
        }

        public bool Delete(User user, int id)
        {
            return _repo.Delete(id,user.Id);
        }
    }
}