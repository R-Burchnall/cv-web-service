﻿using System.Collections.Generic;
using Resumer.Models;

namespace Resumer.Domain.Interface
{
    public interface IAuthService
    {
        User AuthenticateUser(AuthenticationModel authenticationModel);
        string GenerateJwtToken(User user);
        bool RegisterNewUser(AuthenticationModel authenticationModel);
        bool AttemptToSendPasswordReset(string emailAddress);
        IEnumerable<User> ListUsers();
        User GetUserByID(int userId);
    }
}