﻿using System.Collections.Generic;
using Resumer.Models;

namespace Resumer.Domain.Interface
{
    public interface IService<T>
    {
        public IEnumerable<T> List(User user);
        public T Create(User user, T model);
        public T Read(User user, int id);
        public T Update(User user, int id, T model);
        public bool Delete(User user, int id);
    }
}