﻿using System;
using System.Collections.Generic;
using Resumer.Data.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Domain
{
    public class PersonalStatementService: IService<PersonalStatement>
    {
        private readonly IRepository<PersonalStatement> _repo;

        public PersonalStatementService(IRepository<PersonalStatement> repo)
        {
            _repo = repo;
        }

        public IEnumerable<PersonalStatement> List(User user)
        {
            return _repo.List(user.Id);
        }

        public PersonalStatement Create(User user, PersonalStatement model)
        {
            if (model.User != null && model.UserId == 0)
            {
                model.UserId = model.User.Id;
                model.User = null;
            }
            model.CreatedAt = DateTime.Now;
            return _repo.Create(model);
        }

        public PersonalStatement Read(User user, int id)
        {
            return _repo.Read(user.Id);
        }

        public PersonalStatement Update(User user, int id, PersonalStatement model)
        {
            return _repo.Update(id, model);
        }

        public bool Delete(User user, int id)
        {
            return _repo.Delete(id,user.Id);
        }
    }
}