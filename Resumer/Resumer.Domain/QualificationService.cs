﻿using System.Collections.Generic;
using Resumer.Data.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Domain
{
    public class QualificationService: IService<Qualification>
    {
        private readonly IRepository<Qualification> _repo;

        public QualificationService(IRepository<Qualification> repo)
        {
            _repo = repo;
        }

        public IEnumerable<Qualification> List(User user)
        {
            return _repo.List(user.Id);
        }

        public Qualification Create(User user, Qualification model)
        {
            if (model.User != null && model.UserId == 0)
            {
                model.UserId = model.User.Id;
                model.User = null;
            }
            return _repo.Create(model);
        }

        public Qualification Read(User user, int id)
        {
            return _repo.Read(user.Id);
        }

        public Qualification Update(User user, int id, Qualification model)
        {
            return _repo.Update(id, model);
        }

        public bool Delete(User user, int id)
        {
            return _repo.Delete(id,user.Id);
        }
    }
}