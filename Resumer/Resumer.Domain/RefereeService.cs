﻿using System.Collections.Generic;
using Resumer.Data.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Domain
{
    public class RefereeService: IService<Referee>
    {
        private readonly IRepository<Referee> _repo;

        public RefereeService(IRepository<Referee> repo)
        {
            _repo = repo;
        }

        public IEnumerable<Referee> List(User user)
        {
            return _repo.List(user.Id);
        }

        public Referee Create(User user, Referee model)
        {
            if (model.User != null && model.UserId == 0)
            {
                model.UserId = model.User.Id;
                model.User = null;
            }
            return _repo.Create(model);
        }

        public Referee Read(User user, int id)
        {
            return _repo.Read(user.Id);
        }

        public Referee Update(User user, int id, Referee model)
        {
            return _repo.Update(id, model);
        }

        public bool Delete(User user, int id)
        {
            return _repo.Delete(id,user.Id);
        }
    }
}