﻿using System.Collections.Generic;
using Resumer.Data.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Domain
{
    public class SkillService: IService<Skill>
    {
        private readonly IRepository<Skill> _repo;

        public SkillService(IRepository<Skill> repo)
        {
            _repo = repo;
        }

        public IEnumerable<Skill> List(User user)
        {
            return _repo.List(user.Id);
        }

        public Skill Create(User user, Skill model)
        {
            if (model.User != null && model.UserId == 0)
            {
                model.UserId = model.User.Id;
                model.User = null;
            }
            return _repo.Create(model);
        }

        public Skill Read(User user, int id)
        {
            return _repo.Read(user.Id);
        }

        public Skill Update(User user, int id, Skill model)
        {
            return _repo.Update(id, model);
        }

        public bool Delete(User user, int id)
        {
            return _repo.Delete(id,user.Id);
        }
    }
}