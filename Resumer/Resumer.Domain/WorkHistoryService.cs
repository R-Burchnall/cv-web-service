﻿using System.Collections.Generic;
using Resumer.Data.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Domain
{
    public class WorkHistoryService: IService<WorkHistory>
    {
        private readonly IRepository<WorkHistory> _repo;

        public WorkHistoryService(IRepository<WorkHistory> repo)
        {
            _repo = repo;
        }

        public IEnumerable<WorkHistory> List(User user)
        {
            return _repo.List(user.Id);
        }

        public WorkHistory Create(User user, WorkHistory model)
        {
            if (model.User != null && model.UserId == 0)
            {
                model.UserId = model.User.Id;
                model.User = null;
            }
            return _repo.Create(model);
        }

        public WorkHistory Read(User user, int id)
        {
            return _repo.Read(user.Id);
        }

        public WorkHistory Update(User user, int id, WorkHistory model)
        {
            return _repo.Update(id, model);
        }

        public bool Delete(User user, int id)
        {
            return _repo.Delete(id,user.Id);
        }
    }
}