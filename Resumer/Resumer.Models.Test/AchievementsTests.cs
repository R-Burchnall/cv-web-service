﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Models.Tests
{
    [TestClass]
    public class AchievementsTests
    {
        [TestMethod]
        public void Achievement_IsValidModel_WillValidateSuccessfully()
        {
            var result = new List<ValidationResult>();
            var achievement = ModelHelpers.GetValidAchievement();
            var isValid = Validator.TryValidateObject(achievement, new ValidationContext(achievement), result, true);
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void Achievement_IsMissingName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var achievement = ModelHelpers.GetValidAchievement();
            achievement.Name = string.Empty;
            var isValid = Validator.TryValidateObject(achievement, new ValidationContext(achievement), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The Name field is required.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void Achievement_IsLongName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var achievement = ModelHelpers.GetValidAchievement();
            achievement.Name = StringHelpers.RandomString(301);
            var isValid = Validator.TryValidateObject(achievement, new ValidationContext(achievement), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field Name must be a string with a maximum length of 100.",
                result[0].ErrorMessage);
        }

        [TestMethod]
        public void Achievement_IsLongDescription_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var achievement = ModelHelpers.GetValidAchievement();
            achievement.Description = StringHelpers.RandomString(301);
            var isValid = Validator.TryValidateObject(achievement, new ValidationContext(achievement), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field Description must be a string with a maximum length of 300.",
                result[0].ErrorMessage);
        }
    }
}