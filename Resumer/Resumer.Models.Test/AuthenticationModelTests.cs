﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Models.Tests
{
    [TestClass]
    public class AuthenticationModelTests
    {
        [TestMethod]
        public void AuthenticationModel_IsValidModel_WillValidateSuccessfully()
        {
            var result = new List<ValidationResult>();
            var skill = ModelHelpers.GetValidAuthenticationModel();
            var isValid = Validator.TryValidateObject(skill, new ValidationContext(skill), result, true);
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void AuthenticationModel_IsMissingEmailAddress_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var user = ModelHelpers.GetValidAuthenticationModel();
            user.EmailAddress = null;
            var isValid = Validator.TryValidateObject(user, new ValidationContext(user), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The EmailAddress field is required.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void AuthenticationModel_IsInvalidEmailAddress_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var user = ModelHelpers.GetValidAuthenticationModel();
            user.EmailAddress = StringHelpers.RandomString();
            var isValid = Validator.TryValidateObject(user, new ValidationContext(user), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The EmailAddress field is not a valid e-mail address.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void AuthenticationModel_IsMissinPassword_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var user = ModelHelpers.GetValidAuthenticationModel();
            user.Password = null;
            var isValid = Validator.TryValidateObject(user, new ValidationContext(user), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The Password field is required.", result[0].ErrorMessage);
        }
    }
}