﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Models.Tests
{
    [TestClass]
    public class ContactDetailsTests
    {
        [TestMethod]
        public void ContactDetails_IsValidModel_WillValidateSuccessfully()
        {
            var result = new List<ValidationResult>();
            var contactDetails = ModelHelpers.GetValidContactDetails();
            var isValid =
                Validator.TryValidateObject(contactDetails, new ValidationContext(contactDetails), result, true);
            Assert.IsTrue(isValid, isValid ? "Success" : result[0].ErrorMessage);
        }

        [TestMethod]
        public void ContactDetails_IsMissingFirstName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var contactDetails = ModelHelpers.GetValidContactDetails();
            contactDetails.FirstName = string.Empty;
            var isValid =
                Validator.TryValidateObject(contactDetails, new ValidationContext(contactDetails), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The FirstName field is required.",
                result[0].ErrorMessage);
        }

        [TestMethod]
        public void ContactDetails_WithLongFirstName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var contactDetails = ModelHelpers.GetValidContactDetails();
            contactDetails.FirstName = StringHelpers.RandomString(51);
            var isValid =
                Validator.TryValidateObject(contactDetails, new ValidationContext(contactDetails), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field FirstName must be a string with a maximum length of 50.",
                result[0].ErrorMessage);
        }

        [TestMethod]
        public void ContactDetails_IsMissingLastName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var contactDetails = ModelHelpers.GetValidContactDetails();
            contactDetails.LastName = string.Empty;
            var isValid =
                Validator.TryValidateObject(contactDetails, new ValidationContext(contactDetails), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The LastName field is required.",
                result[0].ErrorMessage);
        }

        [TestMethod]
        public void ContactDetails_WithLongLastName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var contactDetails = ModelHelpers.GetValidContactDetails();
            contactDetails.LastName = StringHelpers.RandomString(51);
            var isValid =
                Validator.TryValidateObject(contactDetails, new ValidationContext(contactDetails), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field LastName must be a string with a maximum length of 50.",
                result[0].ErrorMessage);
        }

        [TestMethod]
        public void ContactDetails_WithLongInitials_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var contactDetails = ModelHelpers.GetValidContactDetails();
            contactDetails.Initials = StringHelpers.RandomString(9);
            var isValid =
                Validator.TryValidateObject(contactDetails, new ValidationContext(contactDetails), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field Initials must be a string with a maximum length of 8.",
                result[0].ErrorMessage);
        }

        [DataTestMethod]
        [DataRow("754-3010", true)]
        [DataRow("(541) 754-3010", true)]
        [DataRow("+1-541-754-3010", true)]
        [DataRow("1-541-754-3010", true)]
        [DataRow("01455 614959", true)]
        [DataRow("01455614959", true)]
        [DataRow("001-541-754-3010", true)]
        [DataRow("abc", false)]
        [DataRow("😊😊😊😊😊😊😘😘😘", false)]
        public void ContactDetails_WithPhoneNumber_WillReturnValidationResult(string phoneNumber, bool willPass)
        {
            var result = new List<ValidationResult>();
            var contactDetails = ModelHelpers.GetValidContactDetails();
            contactDetails.PhoneNumber = phoneNumber;
            var isValid =
                Validator.TryValidateObject(contactDetails, new ValidationContext(contactDetails), result, true);

            if (willPass)
            {
                Assert.IsTrue(isValid, isValid ? "Success" : result[0].ErrorMessage);
            }
            else
            {
                Assert.IsFalse(isValid);
                Assert.AreEqual(1, result.Count);
                Assert.AreEqual("The PhoneNumber field is not a valid phone number.",
                    result[0].ErrorMessage);
            }
        }
    }
}