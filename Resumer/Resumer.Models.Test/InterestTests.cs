﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Models.Tests
{
    [TestClass]
    public class InterestTests
    {
        [TestMethod]
        public void Interest_IsValidModel_WillValidateSuccessfully()
        {
            var result = new List<ValidationResult>();
            var interest = ModelHelpers.GetValidInterest();
            var isValid = Validator.TryValidateObject(interest, new ValidationContext(interest), result, true);
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void Interest_IsMissingName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var interest = ModelHelpers.GetValidInterest();
            interest.Name = string.Empty;
            var isValid = Validator.TryValidateObject(interest, new ValidationContext(interest), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The Name field is required.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void Interest_IsLongName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var interest = ModelHelpers.GetValidInterest();
            interest.Name = StringHelpers.RandomString(51);
            var isValid = Validator.TryValidateObject(interest, new ValidationContext(interest), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field Name must be a string with a maximum length of 50.",
                result[0].ErrorMessage);
        }

        [TestMethod]
        public void Interest_IsDescription_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var interest = ModelHelpers.GetValidInterest();
            interest.Description = StringHelpers.RandomString(201);
            var isValid = Validator.TryValidateObject(interest, new ValidationContext(interest), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field Description must be a string with a maximum length of 200.",
                result[0].ErrorMessage);
        }
    }
}