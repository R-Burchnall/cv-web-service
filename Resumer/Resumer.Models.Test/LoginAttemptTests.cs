﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Models.Tests
{
    [TestClass]
    public class LoginAttemptTests
    {
        [TestMethod]
        public void LoginAttempt_IsValidModel_WillValidateSuccessfully()
        {
            var result = new List<ValidationResult>();
            var loginAttempt = ModelHelpers.GetValidLoginAttempt();
            var isValid = Validator.TryValidateObject(loginAttempt, new ValidationContext(loginAttempt), result, true);
            Assert.IsTrue(isValid, loginAttempt.IpAddress);
        }

        [TestMethod]
        public void LoginAttempt_WithInvalidIp_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var loginAttempt = ModelHelpers.GetValidLoginAttempt();
            loginAttempt.IpAddress = StringHelpers.RandomString(12);
            var isValid = Validator.TryValidateObject(loginAttempt, new ValidationContext(loginAttempt), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The IP Address was not in a recognized format.",
                result[0].ErrorMessage);
        }

        [DataTestMethod]
        [DataRow("1.1.1.1")]
        [DataRow("1.10.10.1")]
        [DataRow("10.10.10.10")]
        [DataRow("10.100.100.10")]
        [DataRow("100.100.100.100")]
        [DataRow("1.100.100.100")]
        [DataRow("100.1.100.100")]
        [DataRow("100.100.1.100")]
        [DataRow("100.100.100.1")]
        [DataRow("255.249.255.255")]
        [DataRow("255.255.255.1")]
        public void LoginAttempt_WithVariousIpAddress_WillValidate(string ipAddress)
        {
            var result = new List<ValidationResult>();
            var loginAttempt = ModelHelpers.GetValidLoginAttempt();
            loginAttempt.IpAddress = ipAddress;
            var isValid = Validator.TryValidateObject(loginAttempt, new ValidationContext(loginAttempt), result, true);
            Assert.IsTrue(isValid);
        }

        [DataTestMethod]
        [DataRow("11.1.1")]
        [DataRow("1.10.10.1.10")]
        [DataRow("::1")]
        [DataRow("255.255.255.256")]
        [DataRow("256.1.1.1")]
        [DataRow("192.168.1.1/24")]
        public void LoginAttempt_WithVariousIpAddress_WillReturnInvalid(string ipAddress)
        {
            var result = new List<ValidationResult>();
            var loginAttempt = ModelHelpers.GetValidLoginAttempt();
            loginAttempt.IpAddress = ipAddress;
            var isValid = Validator.TryValidateObject(loginAttempt, new ValidationContext(loginAttempt), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The IP Address was not in a recognized format.",
                result[0].ErrorMessage);
        }
    }
}