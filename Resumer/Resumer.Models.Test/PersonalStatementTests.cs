﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Models.Tests
{
    [TestClass]
    public class PersonalStatementTests
    {
        [TestMethod]
        public void PersonalStatement_IsValidModel_WillValidateSuccessfully()
        {
            var result = new List<ValidationResult>();
            var personalStatement = ModelHelpers.GetValidPersonalStatement();
            var isValid = Validator.TryValidateObject(personalStatement, new ValidationContext(personalStatement),
                result, true);
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void PersonalStatement_IsMissingContent_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var personalStatement = ModelHelpers.GetValidPersonalStatement();
            personalStatement.Content = null;
            var isValid = Validator.TryValidateObject(personalStatement, new ValidationContext(personalStatement),
                result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The Content field is required.", result[0].ErrorMessage);
        }
    }
}