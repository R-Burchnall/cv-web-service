﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Models.Tests
{
    [TestClass]
    public class QualificationTests
    {
        [TestMethod]
        public void Qualification_IsValidModel_WillValidateSuccessfully()
        {
            var result = new List<ValidationResult>();
            var personalStatement = ModelHelpers.GetValidQualification();
            var isValid = Validator.TryValidateObject(personalStatement, new ValidationContext(personalStatement),
                result, true);
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void Qualification_IsMissingName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var personalStatement = ModelHelpers.GetValidQualification();
            personalStatement.Name = string.Empty;
            ;
            var isValid = Validator.TryValidateObject(personalStatement, new ValidationContext(personalStatement),
                result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The Name field is required.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void Qualification_IsMissingGrade_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var personalStatement = ModelHelpers.GetValidQualification();
            personalStatement.Grade = string.Empty;
            var isValid = Validator.TryValidateObject(personalStatement, new ValidationContext(personalStatement),
                result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The Grade field is required.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void Qualification_WithLongName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var personalStatement = ModelHelpers.GetValidQualification();
            personalStatement.Name = StringHelpers.RandomString(201);
            var isValid = Validator.TryValidateObject(personalStatement, new ValidationContext(personalStatement),
                result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field Name must be a string with a maximum length of 200.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void Qualification_WithLongGrade_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var personalStatement = ModelHelpers.GetValidQualification();
            personalStatement.Grade = StringHelpers.RandomString(21);
            var isValid = Validator.TryValidateObject(personalStatement, new ValidationContext(personalStatement),
                result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field Grade must be a string with a maximum length of 20.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void Qualification_WithLongDetails_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var personalStatement = ModelHelpers.GetValidQualification();
            personalStatement.Details = StringHelpers.RandomString(201);
            var isValid = Validator.TryValidateObject(personalStatement, new ValidationContext(personalStatement),
                result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field Details must be a string with a maximum length of 200.", result[0].ErrorMessage);
        }
    }
}