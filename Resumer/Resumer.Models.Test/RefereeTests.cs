﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Models.Tests
{
    [TestClass]
    public class RefereeTests
    {
        [TestMethod]
        public void Referee_IsValidModel_WillValidateSuccessfully()
        {
            var result = new List<ValidationResult>();
            var referee = ModelHelpers.GetValidReferee();
            var isValid = Validator.TryValidateObject(referee, new ValidationContext(referee), result, true);
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void Referee_IsMissingName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var referee = ModelHelpers.GetValidReferee();
            referee.Name = null;
            var isValid = Validator.TryValidateObject(referee, new ValidationContext(referee), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The Name field is required.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void Referee_IsMissingRelationship_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var referee = ModelHelpers.GetValidReferee();
            referee.Relationship = null;
            var isValid = Validator.TryValidateObject(referee, new ValidationContext(referee), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The Relationship field is required.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void Referee_IsMissingJobTitle_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var referee = ModelHelpers.GetValidReferee();
            referee.JobTitle = null;
            var isValid = Validator.TryValidateObject(referee, new ValidationContext(referee), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The JobTitle field is required.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void Referee_IsMissingOrganizationName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var referee = ModelHelpers.GetValidReferee();
            referee.OrganizationName = null;
            var isValid = Validator.TryValidateObject(referee, new ValidationContext(referee), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The OrganizationName field is required.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void Referee_IsMissingEmailAddress_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var referee = ModelHelpers.GetValidReferee();
            referee.EmailAddress = null;
            var isValid = Validator.TryValidateObject(referee, new ValidationContext(referee), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The EmailAddress field is required.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void Referee_WithLongName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var referee = ModelHelpers.GetValidReferee();
            referee.Name = StringHelpers.RandomString(101);
            var isValid = Validator.TryValidateObject(referee, new ValidationContext(referee), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field Name must be a string with a maximum length of 100.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void Referee_WithLongRelationship_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var referee = ModelHelpers.GetValidReferee();
            referee.Relationship = StringHelpers.RandomString(101);
            var isValid = Validator.TryValidateObject(referee, new ValidationContext(referee), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field Relationship must be a string with a maximum length of 100.",
                result[0].ErrorMessage);
        }

        [TestMethod]
        public void Referee_WithLongJobTitle_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var referee = ModelHelpers.GetValidReferee();
            referee.JobTitle = StringHelpers.RandomString(101);
            var isValid = Validator.TryValidateObject(referee, new ValidationContext(referee), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field JobTitle must be a string with a maximum length of 100.",
                result[0].ErrorMessage);
        }

        [TestMethod]
        public void Referee_WithLongOrganizationName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var referee = ModelHelpers.GetValidReferee();
            referee.OrganizationName = StringHelpers.RandomString(101);
            var isValid = Validator.TryValidateObject(referee, new ValidationContext(referee), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field OrganizationName must be a string with a maximum length of 100.",
                result[0].ErrorMessage);
        }

        [TestMethod]
        public void Referee_WithInvalidEmailAddress_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var referee = ModelHelpers.GetValidReferee();
            referee.EmailAddress = StringHelpers.RandomString(101);
            var isValid = Validator.TryValidateObject(referee, new ValidationContext(referee), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The EmailAddress field is not a valid e-mail address.", result[0].ErrorMessage);
        }

        [DataTestMethod]
        [DataRow("754-3010", true)]
        [DataRow("(541) 754-3010", true)]
        [DataRow("+1-541-754-3010", true)]
        [DataRow("1-541-754-3010", true)]
        [DataRow("01455 614959", true)]
        [DataRow("01455614959", true)]
        [DataRow("001-541-754-3010", true)]
        [DataRow("abc", false)]
        [DataRow("😊😊😊😊😊😊😘😘😘", false)]
        public void Referee_WithPhoneNumber_WillReturnValidationResult(string phoneNumber, bool willPass)
        {
            var result = new List<ValidationResult>();
            var referee = ModelHelpers.GetValidReferee();
            referee.PhoneNumber = phoneNumber;
            var isValid =
                Validator.TryValidateObject(referee, new ValidationContext(referee), result, true);

            if (willPass)
            {
                Assert.IsTrue(isValid, isValid ? "Success" : result[0].ErrorMessage);
            }
            else
            {
                Assert.IsFalse(isValid);
                Assert.AreEqual(1, result.Count);
                Assert.AreEqual("The PhoneNumber field is not a valid phone number.",
                    result[0].ErrorMessage);
            }
        }
    }
}