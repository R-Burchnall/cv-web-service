﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Models.Tests
{
    [TestClass]
    public class SkillTests
    {
        [TestMethod]
        public void Skill_IsValidModel_WillValidateSuccessfully()
        {
            var result = new List<ValidationResult>();
            var skill = ModelHelpers.GetValidSkill();
            var isValid = Validator.TryValidateObject(skill, new ValidationContext(skill), result, true);
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void Skill_IsLongName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var skill = ModelHelpers.GetValidSkill();
            skill.Name = StringHelpers.RandomString(101);
            var isValid = Validator.TryValidateObject(skill, new ValidationContext(skill), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field Name must be a string with a maximum length of 100.",
                result[0].ErrorMessage);
        }

        [TestMethod]
        public void Skill_IsMissingName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var skill = ModelHelpers.GetValidSkill();
            skill.Name = string.Empty;
            ;
            var isValid = Validator.TryValidateObject(skill, new ValidationContext(skill), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The Name field is required.", result[0].ErrorMessage);
        }
    }
}