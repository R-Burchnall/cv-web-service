﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Models.Tests
{
    [TestClass]
    public class UserTests
    {
        [TestMethod]
        public void User_IsValidModel_WillValidateSuccessfully()
        {
            var result = new List<ValidationResult>();
            var skill = ModelHelpers.GetValidUser();
            var isValid = Validator.TryValidateObject(skill, new ValidationContext(skill), result, true);
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void User_IsMissingEmailAddress_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var user = ModelHelpers.GetValidUser();
            user.EmailAddress = null;
            var isValid = Validator.TryValidateObject(user, new ValidationContext(user), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The EmailAddress field is required.", result[0].ErrorMessage);
        }
    }
}