﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Resumer.TestHelpers.Helpers;

namespace Resumer.Models.Tests
{
    [TestClass]
    public class WorkHistoryTests
    {
        [TestMethod]
        public void WorkHistory_IsValidModel_WillValidateSuccessfully()
        {
            var result = new List<ValidationResult>();
            var workHistory = ModelHelpers.GetValidWorkHistory();
            var isValid = Validator.TryValidateObject(workHistory, new ValidationContext(workHistory), result, true);
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void WorkHistory_IsMissingJobTitle_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var workHistory = ModelHelpers.GetValidWorkHistory();
            workHistory.JobTitle = null;
            var isValid = Validator.TryValidateObject(workHistory, new ValidationContext(workHistory), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The JobTitle field is required.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void WorkHistory_IsMissingOrganizationName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var workHistory = ModelHelpers.GetValidWorkHistory();
            workHistory.OrganizationName = null;
            var isValid = Validator.TryValidateObject(workHistory, new ValidationContext(workHistory), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The OrganizationName field is required.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void WorkHistory_IsMissingDetails_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var workHistory = ModelHelpers.GetValidWorkHistory();
            workHistory.Details = null;
            var isValid = Validator.TryValidateObject(workHistory, new ValidationContext(workHistory), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The Details field is required.", result[0].ErrorMessage);
        }

        [TestMethod]
        public void WorkHistory_WithLongJobTitle_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var workHistory = ModelHelpers.GetValidWorkHistory();
            workHistory.JobTitle = StringHelpers.RandomString(101);
            var isValid = Validator.TryValidateObject(workHistory, new ValidationContext(workHistory), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field JobTitle must be a string with a maximum length of 100.",
                result[0].ErrorMessage);
        }

        [TestMethod]
        public void WorkHistory_WithLongOrganizationName_WillReturnInvalid()
        {
            var result = new List<ValidationResult>();
            var workHistory = ModelHelpers.GetValidWorkHistory();
            workHistory.OrganizationName = StringHelpers.RandomString(101);
            var isValid = Validator.TryValidateObject(workHistory, new ValidationContext(workHistory), result, true);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The field OrganizationName must be a string with a maximum length of 100.",
                result[0].ErrorMessage);
        }
    }
}