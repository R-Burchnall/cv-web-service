﻿namespace Resumer.Models
{
    public abstract class Address : IAddress
    {
        public string Street { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
    }
}