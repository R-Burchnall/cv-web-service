﻿using System;

namespace Resumer.Models
{
    public abstract class Duration : IDuration
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}