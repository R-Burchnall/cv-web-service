﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace Resumer.Models
{
    public class Achievement
    {
        [Key] public int Id { get; set; }
        [NotNull]
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(300)] public string Description { get; set; }
        
        [Required]  public int UserId { get; set; }
        [Required][JsonIgnore]  public User User { get; set; }

    }
}