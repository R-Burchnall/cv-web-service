﻿using System.ComponentModel.DataAnnotations;

namespace Resumer.Models
{
    public class AuthenticationModel
    {
        [Required] [EmailAddress] public string EmailAddress { get; set; }
        [Required] public string Password { get; set; }
    }
}