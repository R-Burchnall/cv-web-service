﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Resumer.Models
{
    public class ContactDetails : Address
    {
        [Key] public int Id { get; set; }
        [JsonIgnore] public User User { get; set; }
        [Required] public int UserId { get; set; }
        [Required] [StringLength(50)] public string FirstName { get; set; }
        [Required] [StringLength(50)] public string LastName { get; set; }
        [StringLength(8)] public string Initials { get; set; }
        [Phone] public string PhoneNumber { get; set; }
    }
}