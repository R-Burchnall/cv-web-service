﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Resumer.Models
{
    public class Interest
    {
        [Key] public int Id { get; set; }
        [JsonIgnore] public User User { get; set; }
        [Required] public int UserId { get; set; }

        [Required] [StringLength(50)] public string Name { get; set; }
        [StringLength(200)] public string Description { get; set; }
    }
}