﻿namespace Resumer.Models
{
    public interface IAddress
    {
        string Street { get; set; }
        string City { get; set; }
        string County { get; set; }
        string PostalCode { get; set; }
        string Country { get; set; }
    }
}