﻿using System;

namespace Resumer.Models
{
    public interface IDuration
    {
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
    }
}