﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Resumer.Models
{
    public class PersonalStatement
    {
        [Key] public int Id { get; set; }
        [JsonIgnore] public User User { get; set; }
        [Required] public int UserId { get; set; }
        [Required] public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}