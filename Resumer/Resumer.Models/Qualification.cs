﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Resumer.Models
{
    public class Qualification : Duration
    {
        [Key] public int Id { get; set; }
        [JsonIgnore] public User User { get; set; }
        [Required] public int UserId { get; set; }

        [Required] [StringLength(200)] public string Name { get; set; }
        [Required] [Range(1, 8)] public int Level { get; set; }
        [Required] [StringLength(20)] public string Grade { get; set; }
        [StringLength(200)] public string Details { get; set; }
    }
}