﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Resumer.Models
{
    public class Referee
    {
        [Key] public int Id { get; set; }
        [JsonIgnore] public User User { get; set; }
        [Required] public int UserId { get; set; }

        [Required] [StringLength(100)] public string Name { get; set; }
        [Required] [StringLength(100)] public string Relationship { get; set; }
        [Required] [StringLength(100)] public string JobTitle { get; set; }
        [Required] [StringLength(100)] public string OrganizationName { get; set; }
        [Phone] public string PhoneNumber { get; set; }
        [Required] [EmailAddress] public string EmailAddress { get; set; }
    }
}