﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Resumer.Models
{
    public class Skill
    {
        [Key] public int Id { get; set; }
        [JsonIgnore] public User User { get; set; }
        [Required] public int UserId { get; set; }

        [Required] [StringLength(100)] public string Name { get; set; }
    }
}