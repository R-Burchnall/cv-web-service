﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Resumer.Models
{
    public class User
    {
        [Key] public int Id { get; set; }
        [Required] public string EmailAddress { get; set; }
        public string PasswordDigest { get; set; }
        public string ForgottenPasswordToken { get; set; }
        public string ConfirmationCode { get; set; }

        // Related Fields Below
        public List<Achievement> Achievements { get; set; }
        public ContactDetails ContactDetails { get; set; }
        public List<Interest> Interests { get; set; }
        public List<PersonalStatement> PersonalStatements { get; set; }
        public List<LoginAttempt> LoginAttempts { get; set; }
        public List<Qualification> Qualifications { get; set; }
        public List<Skill> Skills { get; set; }
        public List<WorkHistory> WorkHistory { get; set; }
        public List<Referee> Referees { get; set; }
    }
}