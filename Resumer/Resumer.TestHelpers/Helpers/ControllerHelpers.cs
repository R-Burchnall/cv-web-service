﻿using Moq;
using Resumer.Api.Controllers;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.TestHelpers.Helpers
{
    public class ControllerHelpers
    {
        public static AchievementController GetAchievementController(Mock<IService<Achievement>> service = null,
            Mock<IAuthService> authService = null)
        {
            return new AchievementController(
                service != null ? service.Object : new Mock<IService<Achievement>>().Object,
                authService != null ? authService.Object : new Mock<IAuthService>().Object);
        }

        public static InterestController GetInterestController(Mock<IService<Interest>> service = null,
            Mock<IAuthService> authService = null)
        {
            return new InterestController(
                service != null ? service.Object : new Mock<IService<Interest>>().Object,
                authService != null ? authService.Object : new Mock<IAuthService>().Object);
        }

        public static PersonalStatementController GetPersonalStatementController(
            Mock<IService<PersonalStatement>> service = null,
            Mock<IAuthService> authService = null)
        {
            return new PersonalStatementController(
                service != null ? service.Object : new Mock<IService<PersonalStatement>>().Object,
                authService != null ? authService.Object : new Mock<IAuthService>().Object);
        }

        public static QualificationController GetQualificationController(Mock<IService<Qualification>> service = null,
            Mock<IAuthService> authService = null)
        {
            return new QualificationController(
                service != null ? service.Object : new Mock<IService<Qualification>>().Object,
                authService != null ? authService.Object : new Mock<IAuthService>().Object);
        }

        public static RefereeController GetRefereeController(Mock<IService<Referee>> service = null,
            Mock<IAuthService> authService = null)
        {
            return new RefereeController(
                service != null ? service.Object : new Mock<IService<Referee>>().Object,
                authService != null ? authService.Object : new Mock<IAuthService>().Object);
        }

        public static SkillController GetSkillController(Mock<IService<Skill>> service = null,
            Mock<IAuthService> authService = null)
        {
            return new SkillController(
                service != null ? service.Object : new Mock<IService<Skill>>().Object,
                authService != null ? authService.Object : new Mock<IAuthService>().Object);
        }

        public static WorkHistoryController GetWorkHistoryController(Mock<IService<WorkHistory>> service = null,
            Mock<IAuthService> authService = null)
        {
            return new WorkHistoryController(
                service != null ? service.Object : new Mock<IService<WorkHistory>>().Object,
                authService != null ? authService.Object : new Mock<IAuthService>().Object);
        }

        public static AuthenticationController GetAuthenticationController(Mock<IAuthService> service = null)
        {
            return new AuthenticationController(
                service != null ? service.Object : new Mock<IAuthService>().Object);
        }
    }
}