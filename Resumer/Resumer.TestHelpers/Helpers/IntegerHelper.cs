﻿using System;

namespace Resumer.TestHelpers.Helpers
{
    public static class IntegerHelper
    {
        private static readonly Random _random = new Random();

        public static int RandomInt(int length = 1)
        {
            return (int) (_random.NextDouble() * Math.Pow(10, length));
        }

        public static int RandomInclusiveBetween(int from, int to)
        {
            return _random.Next(from, to + 1);
        }
    }
}