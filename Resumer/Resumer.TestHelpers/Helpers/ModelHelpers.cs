﻿using System;
using Resumer.Models;

namespace Resumer.TestHelpers.Helpers
{
    public static class ModelHelpers
    {
        public static Achievement GetValidAchievement()
        {
            var validUser = GetValidUser();
            return new Achievement
            {
                Description = StringHelpers.RandomString(100),
                Name = StringHelpers.RandomString(),
                User = validUser,
            };
        }

        public static ContactDetails GetValidContactDetails()
        {
            var validUser = GetValidUser();
            return new ContactDetails
            {
                City = StringHelpers.RandomString(),
                Country = StringHelpers.RandomString(),
                County = StringHelpers.RandomString(),
                FirstName = StringHelpers.RandomString(),
                Initials = StringHelpers.RandomString(2),
                LastName = StringHelpers.RandomString(),
                PostalCode = StringHelpers.RandomString(6),
                Street = StringHelpers.RandomString(),
                User = validUser,
            };
        }

        public static Interest GetValidInterest()
        {
            var validUser = GetValidUser();
            return new Interest
            {
                Description = StringHelpers.RandomString(100),
                Name = StringHelpers.RandomString(),
                User = validUser
            };
        }

        public static LoginAttempt GetValidLoginAttempt()
        {
            var validUser = GetValidUser();
            return new LoginAttempt
            {
                AccessTime = DateTime.Now,
                IpAddress = StringHelpers.RandomIP(),
                User = validUser,
            };
        }

        public static PersonalStatement GetValidPersonalStatement()
        {
            var validUser = GetValidUser();
            return new PersonalStatement
            {
                Content = StringHelpers.RandomString(100),
                CreatedAt = DateTime.Now,
                User = validUser,
            };
        }

        public static Qualification GetValidQualification()
        {
            var validUser = GetValidUser();
            var startDate = DateTime.Now.AddMonths(-7);
            var endDate = DateTime.Now.AddMonths(7);
            return new Qualification
            {
                Details = StringHelpers.RandomString(100),
                EndDate = endDate,
                StartDate = startDate,
                Name = StringHelpers.RandomString(),
                Grade = StringHelpers.RandomString(),
                Level = IntegerHelper.RandomInclusiveBetween(1, 8),
                User = validUser,
            };
        }

        public static Referee GetValidReferee()
        {
            var validUser = GetValidUser();
            return new Referee
            {
                EmailAddress = StringHelpers.RandomEmailAddress(),
                JobTitle = StringHelpers.RandomString(),
                Name = StringHelpers.RandomString(),
                OrganizationName = StringHelpers.RandomString(),
                Relationship = StringHelpers.RandomString(),
                User = validUser,
            };
        }

        public static Skill GetValidSkill()
        {
            var validUser = GetValidUser();
            return new Skill
            {
                Name = StringHelpers.RandomString(),
                User = validUser,
            };
        }

        public static User GetValidUser()
        {
            return new User
            {
                EmailAddress = StringHelpers.RandomEmailAddress(),
                PasswordDigest = StringHelpers.RandomString()
            };
        }

        public static WorkHistory GetValidWorkHistory()
        {
            var validUser = GetValidUser();
            var startDate = DateTime.Now.AddMonths(-7);
            var endDate = DateTime.Now.AddMonths(7);
            return new WorkHistory
            {
                EndDate = endDate,
                StartDate = startDate,
                OrganizationName = StringHelpers.RandomString(),
                JobTitle = StringHelpers.RandomString(),
                City = StringHelpers.RandomString(),
                Details = StringHelpers.RandomString(),
                Country = StringHelpers.RandomString(),
                County = StringHelpers.RandomString(),
                PostalCode = StringHelpers.RandomString(6),
                Street = StringHelpers.RandomString(),
                User = validUser,
            };
        }

        public static AuthenticationModel GetValidAuthenticationModel()
        {
            return new AuthenticationModel
            {
                EmailAddress = StringHelpers.RandomEmailAddress(),
                Password = StringHelpers.RandomString()
            };
        }
    }
}