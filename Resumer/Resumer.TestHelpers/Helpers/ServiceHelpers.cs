﻿using Moq;
using Resumer.Data.Interfaces;
using Resumer.Domain;
using Resumer.Models;

namespace Resumer.TestHelpers.Helpers
{
    public class ServiceHelpers
    {
        public static AchievementService GetAchievementService(Mock<IRepository<Achievement>> service = null)
        {
            return new AchievementService(
                service != null ? service.Object : new Mock<IRepository<Achievement>>().Object);
        }

        public static InterestService GetInterestService(Mock<IRepository<Interest>> service = null)
        {
            return new InterestService(
                service != null ? service.Object : new Mock<IRepository<Interest>>().Object);
        }

        public static PersonalStatementService GetPersonalStatementService(
            Mock<IRepository<PersonalStatement>> service = null)
        {
            return new PersonalStatementService(
                service != null ? service.Object : new Mock<IRepository<PersonalStatement>>().Object);
        }

        public static QualificationService GetQualificationService(Mock<IRepository<Qualification>> service = null)
        {
            return new QualificationService(
                service != null ? service.Object : new Mock<IRepository<Qualification>>().Object);
        }

        public static RefereeService GetRefereeService(Mock<IRepository<Referee>> service = null)
        {
            return new RefereeService(
                service != null ? service.Object : new Mock<IRepository<Referee>>().Object);
        }

        public static SkillService GetSkillService(Mock<IRepository<Skill>> service = null)
        {
            return new SkillService(
                service != null ? service.Object : new Mock<IRepository<Skill>>().Object);
        }

        public static WorkHistoryService GetWorkHistoryService(Mock<IRepository<WorkHistory>> service = null)
        {
            return new WorkHistoryService(
                service != null ? service.Object : new Mock<IRepository<WorkHistory>>().Object);
        }
    }
}