﻿using System;
using System.Text;

namespace Resumer.TestHelpers.Helpers
{
    public static class StringHelpers
    {
        private static readonly Random _random = new Random();

        public static string RandomString(int size = 10, bool lowerCase = false)
        {
            var builder = new StringBuilder(size);

            var offset = lowerCase ? 'a' : 'A';
            const int lettersOffset = 26;

            for (var i = 0; i < size; i++)
            {
                var @char = (char) _random.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }

            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }

        public static string RandomIP()
        {
            return
                $"{IntegerHelper.RandomInt(3) % 255}.{IntegerHelper.RandomInt(3) % 255}.{IntegerHelper.RandomInt(3) % 255}.{IntegerHelper.RandomInt(3) % 255}";
        }

        public static string RandomEmailAddress()
        {
            return $"{RandomString()}@{RandomString()}.com";
        }
    }
}