﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Resumer.Api.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class AchievementController : ControllerBase, IController<Achievement>
    {
        private readonly IService<Achievement> _achievementService;

        public AchievementController(IService<Achievement> achievementService, IAuthService authService) : base(authService)
        {
            _achievementService = achievementService;
        }


        [HttpGet]
        public IActionResult Index()
        {
            var list = _achievementService.List(new User());
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Read(int id)
        {
            var model = _achievementService.Read(new User(), id);
            if (model != null) return Ok(model);

            return NotFound("Unable to find entity with that Id");
        }

        [HttpPost]
        public IActionResult Create([FromBody] Achievement achievement)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_achievementService.Create(new User(), achievement));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Achievement achievement)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_achievementService.Update(new User(), id, achievement));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                return Ok(_achievementService.Delete(new User(), id));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}