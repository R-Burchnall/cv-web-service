﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Resumer.Domain.Interface;
using Resumer.Models;
using DateTime = System.DateTime;
using Guid = System.Guid;

namespace Resumer.Api.Controllers
{
    [ApiController]
    [AllowAnonymous]
    [Route("[controller]")]
    public class AuthenticationController : Controller
    {
        private readonly IAuthService _authService;

        public AuthenticationController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("register")]
        public IActionResult Register(AuthenticationModel authenticationModel)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var successful = _authService.RegisterNewUser(authenticationModel);
            if (successful) return Ok();
            return BadRequest("Was not able to create user");
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] AuthenticationModel authenticationModel)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            User user = _authService.AuthenticateUser(authenticationModel);
            if (user != null)
            {
                var tokenString = _authService.GenerateJwtToken(user);
                return Ok(new
                {
                    token = tokenString,
                    userDetails = user,
                });
            }

            return Unauthorized();
        }

        [HttpPost("forgotten-password")]
        public IActionResult ForgottenPassword([FromBody] string emailAddress)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            List<string> errors = new List<string>();
            var successful = _authService.AttemptToSendPasswordReset(emailAddress);
            if (successful) return Ok();
            return BadRequest(errors);
        }
    }
}