﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Resumer.Api.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class ContactDetailsController : ControllerBase, IController<ContactDetails>
    {
        private readonly IService<ContactDetails> _contactDetailsService;

        public ContactDetailsController(IService<ContactDetails> contactDetailsService, IAuthService authService) : base(authService)
        {
            _contactDetailsService = contactDetailsService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var list = _contactDetailsService.List(new User());
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Read(int id)
        {
            var model = _contactDetailsService.Read(new User(), id);
            if (model != null) return Ok(model);

            return NotFound("Unable to find entity with that Id");
        }

        [HttpPost]
        public IActionResult Create(ContactDetails contactDetails)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_contactDetailsService.Create(new User(), contactDetails));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromRoute] int id, ContactDetails contactDetails)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_contactDetailsService.Update(new User(), id, contactDetails));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                return Ok(_contactDetailsService.Delete(new User(), id));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}