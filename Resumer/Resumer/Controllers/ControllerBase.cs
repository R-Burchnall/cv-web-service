﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Api.Controllers
{
    public class ControllerBase : Controller
    {
        private readonly IAuthService _authService;
        protected User _user;
        
        public ControllerBase(IAuthService authService)
        {
            _authService = authService;
        }

        protected User ResolveUser()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "userId");
            _user = _authService.GetUserByID(int.Parse(claim.Value));
            return _user;
        }
    }
}