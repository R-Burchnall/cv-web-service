﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Resumer.Api.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class InterestController : ControllerBase, IController<Interest>
    {
        private readonly IService<Interest> _interestService;

        public InterestController(IService<Interest> interestService, IAuthService authService) : base(authService)
        {
            _interestService = interestService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var list = _interestService.List(new User());
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Read(int id)
        {
            var model = _interestService.Read(new User(), id);
            if (model != null) return Ok(model);

            return NotFound("Unable to find entity with that Id");
        }

        [HttpPost]
        public IActionResult Create(Interest interest)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_interestService.Create(new User(), interest));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromRoute] int id, Interest interest)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_interestService.Update(new User(), id, interest));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
            
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                return Ok(_interestService.Delete(new User(), id));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }        }
    }
}