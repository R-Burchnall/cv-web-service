﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Resumer.Api.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class PersonalStatementController : ControllerBase, IController<PersonalStatement>
    {
        private readonly IService<PersonalStatement> _personalStatementService;

        public PersonalStatementController(IService<PersonalStatement> personalStatementService, IAuthService authService) : base(authService)
        {
            _personalStatementService = personalStatementService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var list = _personalStatementService.List(new User());
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Read(int id)
        {
            var model = _personalStatementService.Read(new User(), id);
            if (model != null) return Ok(model);

            return NotFound("Unable to find entity with that Id");
        }

        [HttpPost]
        public IActionResult Create(PersonalStatement personalStatement)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_personalStatementService.Create(new User(), personalStatement));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromRoute] int id, PersonalStatement personalStatement)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_personalStatementService.Update(new User(), id, personalStatement));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                return Ok(_personalStatementService.Delete(new User(), id));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }        }
    }
}