﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Resumer.Api.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class QualificationController : ControllerBase, IController<Qualification>
    {
        private readonly IService<Qualification> _qualificationService;

        public QualificationController(IService<Qualification> qualificationService, IAuthService authService) : base(authService)
        {
            _qualificationService = qualificationService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var list = _qualificationService.List(new User());
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Read(int id)
        {
            var model = _qualificationService.Read(new User(), id);
            if (model != null) return Ok(model);

            return NotFound("Unable to find entity with that Id");
        }

        [HttpPost]
        public IActionResult Create(Qualification qualification)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_qualificationService.Create(new User(), qualification));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromRoute] int id, Qualification qualification)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_qualificationService.Update(new User(), id, qualification));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
            
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                return Ok(_qualificationService.Delete(new User(), id));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }        }
    }
}