﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Resumer.Api.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class RefereeController : ControllerBase, IController<Referee>
    {
        private readonly IService<Referee> _refereeService;

        public RefereeController(IService<Referee> refereeService, IAuthService authService) : base(authService)
        {
            _refereeService = refereeService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var list = _refereeService.List(new User());
            return Ok(list);
        }

        [HttpGet("id")]
        public IActionResult Read(int id)
        {
            var model = _refereeService.Read(new User(), id);
            if (model != null) return Ok(model);

            return NotFound("Unable to find entity with that Id");
        }

        [HttpPost]
        public IActionResult Create(Referee referee)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            try
            {
                return Ok(_refereeService.Create(new User(), referee));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromRoute] int id, Referee referee)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_refereeService.Update(new User(), id, referee));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                return Ok(_refereeService.Delete(new User(), id));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }        }
    }
}