﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Resumer.Api.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class SkillController : ControllerBase, IController<Skill>
    {
        private readonly IService<Skill> _skillService;

        public SkillController(IService<Skill> skillService, IAuthService authService) : base(authService)
        {
            _skillService = skillService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var list = _skillService.List(new User());
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Read(int id)
        {
            var model = _skillService.Read(new User(), id);
            if (model != null) return Ok(model);

            return NotFound("Unable to find entity with that Id");
        }

        [HttpPost]
        public IActionResult Create(Skill skill)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_skillService.Create(new User(), skill));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromRoute] int id, Skill skill)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_skillService.Update(new User(), id, skill));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                return Ok(_skillService.Delete(new User(), id));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}