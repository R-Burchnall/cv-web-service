﻿﻿using System;
 using System.Linq;
 using Microsoft.AspNetCore.Authentication;
 using Microsoft.AspNetCore.Authorization;
 using Microsoft.AspNetCore.Mvc;
 using Resumer.Domain.Interface;
 using Resumer.Models;

 namespace Resumer.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IAuthService _authService;

        public UserController(IAuthService authService) : base(authService)
        {
            _authService = authService;
        }

        [HttpGet]
        public IActionResult Read()
        {
            var user = ResolveUser();
            return Ok(_authService.GetUserByID(user.Id));
        }
    }
}