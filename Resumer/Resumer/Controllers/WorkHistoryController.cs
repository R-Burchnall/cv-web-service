﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Resumer.Api.Interfaces;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class WorkHistoryController : ControllerBase, IController<WorkHistory>
    {
        private readonly IService<WorkHistory> _workHistoryService;

        public WorkHistoryController(IService<WorkHistory> workHistoryService, IAuthService authService) : base(authService)
        {
            _workHistoryService = workHistoryService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var list = _workHistoryService.List(new User());
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Read(int id)
        {
            var model = _workHistoryService.Read(new User(), id);
            if (model != null) return Ok(model);

            return NotFound("Unable to find entity with that Id");
        }

        [HttpPost]
        public IActionResult Create(WorkHistory workHistory)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_workHistoryService.Create(new User(), workHistory));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromRoute] int id, WorkHistory workHistory)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                return Ok(_workHistoryService.Update(new User(), id, workHistory));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                return Ok(_workHistoryService.Delete(new User(), id));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}