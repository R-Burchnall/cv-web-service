﻿using Microsoft.Extensions.DependencyInjection;
using Resumer.Data;
using Resumer.Data.Interfaces;
using Resumer.Domain;
using Resumer.Domain.Interface;
using Resumer.Models;

namespace Resumer.Api
{
    public static class DependencyRegister
    {
        public static void Register(IServiceCollection services)
        {
            services.AddTransient(typeof(IService<Achievement>), typeof(AchievementService));
            services.AddTransient(typeof(IService<Interest>), typeof(InterestService));
            services.AddTransient(typeof(IService<PersonalStatement>), typeof(PersonalStatementService));
            services.AddTransient(typeof(IService<Qualification>), typeof(QualificationService));
            services.AddTransient(typeof(IService<Referee>), typeof(RefereeService));
            services.AddTransient(typeof(IService<Skill>), typeof(SkillService));
            services.AddTransient(typeof(IService<WorkHistory>), typeof(WorkHistoryService));
            services.AddTransient(typeof(IService<ContactDetails>), typeof(ContactDetailsService));
            services.AddTransient(typeof(IAuthService), typeof(AuthenticationService));
            services.AddTransient(typeof(IRepository<Achievement>), typeof(AchievementRepository));
            services.AddTransient(typeof(IRepository<Interest>), typeof(InterestRepository));
            services.AddTransient(typeof(IRepository<PersonalStatement>), typeof(PersonalStatementRepository));
            services.AddTransient(typeof(IRepository<Qualification>), typeof(QualificationRepository));
            services.AddTransient(typeof(IRepository<Referee>), typeof(RefereeRepository));
            services.AddTransient(typeof(IRepository<Skill>), typeof(SkillRepository));
            services.AddTransient(typeof(IRepository<WorkHistory>), typeof(WorkHistoryRepository));
            services.AddTransient(typeof(IRepository<ContactDetails>), typeof(ContactDetailsRepository));
            services.AddTransient(typeof(IUserRepository), typeof(UserRepository));
        }
    }
}