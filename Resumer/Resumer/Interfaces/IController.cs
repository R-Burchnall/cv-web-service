﻿using Microsoft.AspNetCore.Mvc;

namespace Resumer.Api.Interfaces
{
    public interface IController<T>
    {
        IActionResult Index();
        IActionResult Read(int id);
        IActionResult Create(T model);
        IActionResult Update(int id, T model);
        IActionResult Delete(int id);
    }
}