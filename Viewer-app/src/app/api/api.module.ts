/* tslint:disable */
import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConfiguration, ApiConfigurationParams } from './api-configuration';

import { AchievementService } from './services/achievement.service';
import { AuthenticationService } from './services/authentication.service';
import { ContactDetailsService } from './services/contact-details.service';
import { InterestService } from './services/interest.service';
import { PersonalStatementService } from './services/personal-statement.service';
import { QualificationService } from './services/qualification.service';
import { RefereeService } from './services/referee.service';
import { SkillService } from './services/skill.service';
import { UserService } from './services/user.service';
import { WorkHistoryService } from './services/work-history.service';

/**
 * Module that provides all services and configuration.
 */
@NgModule({
  imports: [],
  exports: [],
  declarations: [],
  providers: [
    AchievementService,
    AuthenticationService,
    ContactDetailsService,
    InterestService,
    PersonalStatementService,
    QualificationService,
    RefereeService,
    SkillService,
    UserService,
    WorkHistoryService,
    ApiConfiguration
  ],
})
export class ApiModule {
  static forRoot(params: ApiConfigurationParams): ModuleWithProviders<ApiModule> {
    return {
      ngModule: ApiModule,
      providers: [
        {
          provide: ApiConfiguration,
          useValue: params
        }
      ]
    }
  }

  constructor( 
    @Optional() @SkipSelf() parentModule: ApiModule,
    @Optional() http: HttpClient
  ) {
    if (parentModule) {
      throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
    }
    if (!http) {
      throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
      'See also https://github.com/angular/angular/issues/20575');
    }
  }
}
