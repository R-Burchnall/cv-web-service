/* tslint:disable */
import { User } from './user';
export interface ContactDetails {
  city?: null | string;
  country?: null | string;
  county?: null | string;
  firstName: string;
  id?: number;
  initials?: null | string;
  lastName: string;
  phoneNumber?: null | string;
  postalCode?: null | string;
  street?: null | string;
  user: User;
  userId: number;
}
