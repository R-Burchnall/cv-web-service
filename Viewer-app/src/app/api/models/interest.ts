/* tslint:disable */
import { User } from './user';
export interface Interest {
  description?: null | string;
  id?: number;
  name: string;
  user: User;
  userId: number;
}
