/* tslint:disable */
import { User } from './user';
export interface LoginAttempt {
  accessTime: string;
  id?: number;
  ipAddress: string;
  user: User;
  userId: number;
}
