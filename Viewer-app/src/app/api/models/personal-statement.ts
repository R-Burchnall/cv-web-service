/* tslint:disable */
import { User } from './user';
export interface PersonalStatement {
  content: string;
  createdAt?: string;
  id?: number;
  user?: User;
  userId: number;
}
