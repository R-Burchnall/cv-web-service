/* tslint:disable */
import { User } from './user';
export interface Qualification {
  details?: null | string;
  endDate?: string;
  grade: string;
  id?: number;
  level: number;
  name: string;
  startDate?: string;
  user: User;
  userId: number;
}
