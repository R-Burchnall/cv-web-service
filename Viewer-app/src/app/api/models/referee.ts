/* tslint:disable */
import { User } from './user';
export interface Referee {
  emailAddress: string;
  id?: number;
  jobTitle: string;
  name: string;
  organizationName: string;
  phoneNumber?: null | string;
  relationship: string;
  user: User;
  userId: number;
}
