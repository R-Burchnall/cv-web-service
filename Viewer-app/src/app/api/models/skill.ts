/* tslint:disable */
import { User } from './user';
export interface Skill {
  id?: number;
  name: string;
  user: User;
  userId: number;
}
