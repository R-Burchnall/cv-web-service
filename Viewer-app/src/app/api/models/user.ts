/* tslint:disable */
import { Achievement } from './achievement';
import { ContactDetails } from './contact-details';
import { Interest } from './interest';
import { LoginAttempt } from './login-attempt';
import { PersonalStatement } from './personal-statement';
import { Qualification } from './qualification';
import { Referee } from './referee';
import { Skill } from './skill';
import { WorkHistory } from './work-history';
export interface User {
  achievements?: null | Array<Achievement>;
  confirmationCode?: null | string;
  contactDetails?: null | ContactDetails;
  emailAddress: string;
  forgottenPasswordToken?: null | string;
  id?: number;
  interests?: null | Array<Interest>;
  loginAttempts?: null | Array<LoginAttempt>;
  passwordDigest?: null | string;
  personalStatements?: null | Array<PersonalStatement>;
  qualifications?: null | Array<Qualification>;
  referees?: null | Array<Referee>;
  skills?: null | Array<Skill>;
  workHistory?: null | Array<WorkHistory>;
}
