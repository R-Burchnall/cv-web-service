/* tslint:disable */
import { User } from './user';
export interface WorkHistory {
  city?: null | string;
  country?: null | string;
  county?: null | string;
  details: string;
  endDate: string;
  id?: number;
  jobTitle: string;
  organizationName: string;
  postalCode?: null | string;
  startDate: string;
  street?: null | string;
  user: User;
  userId: number;
}
