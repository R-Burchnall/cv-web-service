import {Component, OnInit} from '@angular/core';
import {ContactDetails} from './api/models/contact-details';
import {User} from './api/models/user';
import {PersonalStatement} from './api/models/personal-statement';
import {WorkHistory} from './api/models/work-history';
import {Qualification} from './api/models/qualification';
import {Interest} from './api/models/interest';
import {Referee, Skill} from './api/models';
import {AuthenticationService} from './api/services/authentication.service';
import {JWTBody} from './models/Token';
import {ContactDetailsService} from './api/services/contact-details.service';
import {PersonalStatementService} from './api/services/personal-statement.service';
import {SkillService} from './api/services/skill.service';
import {QualificationService} from './api/services/qualification.service';
import {InterestService} from './api/services/interest.service';
import {RefereeService} from './api/services/referee.service';
import {WorkHistoryService} from './api/services/work-history.service';
import {UserService} from './api/services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService,
              private contactDetailsSerice: ContactDetailsService,
              private userService: UserService,
              private personalStatementService: PersonalStatementService,
              private skillService: SkillService,
              private qualificationService: QualificationService,
              private interestService: InterestService,
              private refereeService: RefereeService,
              private workHistoryService: WorkHistoryService) {
  }

  static token = '';
  static Requests: string[] = [];

  title = 'Viewer-app';
  contactDetails: ContactDetails;
  user: User;
  personalStatement: PersonalStatement[] = [];
  workHistories: WorkHistory[] = [];
  qualifications: Qualification[] = [];
  interests: Interest[] = [];
  referees: Referee[] = [];
  skills: Skill[] = [];

  // Login
  hasAttemptedToLogin = false;
  loginError = '';
  loginEmailAddress: string;
  loginPassword: string;
  newPersonalStatement: PersonalStatement = {content: '', userId: 0};
  newWorkHistory: WorkHistory = {details: '', endDate: '', jobTitle: '', organizationName: '', startDate: '', user: undefined, userId: 0};
  newSkill: Skill = {name: '', user: undefined, userId: 0};
  newQualification: Qualification = {grade: '', level: 0, name: '', user: undefined, userId: 0};
  newInterest: Interest = {name: '', user: undefined, userId: 0};
  newReferee: Referee = {emailAddress: '', jobTitle: '', name: '', organizationName: '', relationship: '', user: undefined, userId: 0};
  isLoggedIn = false;

  GetAddress(): string {
    return `${this.contactDetails.street}, ${this.contactDetails.city}, ${this.contactDetails.county}, ${this.contactDetails.postalCode}, ${this.contactDetails.country}`;
  }

  GetRequests(): string[] {
    return AppComponent.Requests;
  }

  ngOnInit(): void {
    this.contactDetails = {
      city: '',
      country: '',
      county: '',
      firstName: '',
      id: 0,
      initials: '',
      lastName: '',
      phoneNumber: '',
      postalCode: '',
      street: '',
      user: null,
      userId: 0,
    };

    this.user = {
      emailAddress: ''
    };

    this.personalStatement = [];
  }

  AttemptToLogIn(): void {
    this.hasAttemptedToLogin = true;
    this.authenticationService.authenticationLoginPost({
      body: {
        emailAddress: this.loginEmailAddress,
        password: this.loginPassword
      }
    }).subscribe((data: any) => {
      const jwtBody = JSON.parse(data) as unknown as JWTBody;
      console.log(jwtBody);
      AppComponent.token = jwtBody.token;
      this.isLoggedIn = true;
      this.user = jwtBody.userDetails;
      try {
        this.personalStatement = this.user.personalStatements;
      } catch {
      }
      try {
        this.contactDetails = this.user.contactDetails;
      } catch {
      }
      try {
        this.workHistories = this.user.workHistory;
      } catch {
      }
      try {
        this.qualifications = this.user.qualifications;
      } catch {
      }
      try {
        this.interests = this.user.interests;
      } catch {
      }
      try {
        this.referees = this.user.referees;
      } catch {
      }
      try {
        this.skills = this.user.skills;
      } catch {
      }
    }, error => console.error(error));
  }

  GetFirstPersonalStatement(): string {
    try {
      return this.personalStatement[0].content;
    } catch (e) {
      return '';
    }
  }

  UpdateContactDetails(): void {
    this.contactDetails.userId = this.user.id;
    if (this.contactDetails.id === 0) {
      this.contactDetailsSerice.contactDetailsPost({
        body: this.contactDetails
      }).subscribe((data: any) => {
        this.contactDetails = data as unknown as ContactDetails;
      }, error => console.error(error));
    } else {
      this.contactDetailsSerice.contactDetailsIdPut({
        id: this.contactDetails.id, body: this.contactDetails
      }).subscribe((data: any) => {
        this.contactDetails = JSON.parse(data) as unknown as ContactDetails;
      }, error => console.error(error));
    }
  }

  UpdatePersonalStatement(): void {
    this.newPersonalStatement.userId = this.user.id;
    this.personalStatementService.personalStatementPost({body: this.newPersonalStatement}).subscribe((data: any) => {
      this.UpdateUser();
    }, error => console.error(error));
  }

  CreateWorkHistory(): void {
    this.newWorkHistory.userId = this.user.id;
    this.workHistoryService.workHistoryPost({body: this.newWorkHistory}).subscribe((data: any) => {
      this.UpdateUser();
    }, error => console.error(error));
  }

  CreateNewQualification(): void {
    this.newQualification.userId = this.user.id;
    this.qualificationService.qualificationPost({body: this.newQualification}).subscribe((data: any) => {
      this.UpdateUser();
    }, error => console.error(error));
  }

  CreateNewWorkHistory(): void {
    this.newWorkHistory.userId = this.user.id;
    this.workHistoryService.workHistoryPost({body: this.newWorkHistory}).subscribe((data: any) => {
      this.UpdateUser();
    }, error => console.error(error));
  }

  CreateNewReferee(): void {
    this.newReferee.userId = this.user.id;
    this.refereeService.refereePost({body: this.newReferee}).subscribe((data: any) => {
      this.UpdateUser();
    }, error => console.error(error));
  }

  CreateNewInterest(): void {
    this.newInterest.userId = this.user.id;
    this.interestService.interestPost({body: this.newInterest}).subscribe((data: any) => {
      this.UpdateUser();
    }, error => console.error(error));
  }

  private UpdateUser(): void {
    this.userService.userGet().subscribe((data: any) => {
      this.user = JSON.parse(data) as unknown as User;
      try {
        this.personalStatement = this.user.personalStatements;
      } catch {
      }
      try {
        this.contactDetails = this.user.contactDetails;
      } catch {
      }
      try {
        this.workHistories = this.user.workHistory;
      } catch {
      }
      try {
        this.qualifications = this.user.qualifications;
      } catch {
      }
      try {
        this.interests = this.user.interests;
      } catch {
      }
      try {
        this.referees = this.user.referees;
      } catch {
      }
      try {
        this.skills = this.user.skills;
      } catch {
      }
    });
  }

  CreateNewSkill(): void {
    this.newSkill.userId = this.user.id;
    this.skillService.skillPost({body: this.newSkill})
      .subscribe((data: any) => {
        this.UpdateUser();
      }, error => console.error(error));
  }

  DeleteReferee(id: number): void {
    this.refereeService.refereeIdDelete({id}).subscribe((data) => {
      this.UpdateUser();
    });
  }

  DeleteInterest(id: number): void {
    this.interestService.interestIdDelete({id}).subscribe((data) => {
      this.UpdateUser();
    });
  }

  DeleteQualifications(id: number): void {
    this.qualificationService.qualificationIdDelete({id}).subscribe((data) => {
      this.UpdateUser();
    });
  }

  DeleteSkill(id: number): void {
    this.skillService.skillIdDelete({id}).subscribe((data) => {
      this.UpdateUser();
    });
  }

  DeleteWorkHistory(id: number): void {
    this.workHistoryService.workHistoryIdDelete({id}).subscribe((data) => {
      this.UpdateUser();
    });
  }
}
