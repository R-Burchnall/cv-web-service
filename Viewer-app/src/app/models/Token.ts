import {User} from '../api/models';

export class JWTBody {
 token: string;
 userDetails: User;
}
