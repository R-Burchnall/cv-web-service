# Resumer - A CV Web Service

A web service to store and allow manipulation of parts of users' CVs in a structured form. It provides a well-documented api allowing users's to create and amend their CV's 

## More Detail
This is a web service written for BJSS as an interview peice. The application features examples of the following;
* Dependency inversion
* Api Controllers
* Inheritance
* Generic Interface types
* Unit Testing
* Mocking/Setting up/Verifying test frameworks
* Model validation techniques
* ASP .Net Indentity intergrations
* Sqlite Database interactivity.

## Getting Started

In order to build the project. You will need to open the solution in either Visual Studio or Rider. After opening the project, restore the nuget packages if that was not done automaticly. Either choose to build or run the project from the IDE.

### Prerequisites

#### Installing .Net Core 3.1
Please go to the following page to install .Net Core, platform specific instructions are provided on that page; https://dotnet.microsoft.com/download/dotnet-core/3.1

#### Installing Node.js (Optional for using the companion app)
Please go to the following page to install Node.js. Please also note that this is only required to build/view the companion app. https://nodejs.org/en/download/

## Accessing swagger UI
After running the application, a page will be available providing documented access to the api. This page features a "try now" function. Using this function you can, for example, create the required user for the Api.

Swagger UI Route by default: https://localhost:44397/swagger/index.html

Swagger Specification File by default: https://localhost:44397/swagger/v1/swagger.json

**NOTE:** The swagger specification is importable into http test clients like postman. This is an effective way to test the api.

## Building the application

### Building the API
After opening the project in the IDE, building the C# Resumer.API project for release will create a bin directory, where the files will be placed. 
That path might look like the following:

 `...CV Webservice\Resumer\Resumer\bin\Release\netcoreapp3.1`

 Within this directory there will be an EXE (on windows) which will be executable. Execting this EXE will create a kestral server in which the api will run.


## Running the companion app

### Building the app
Install the angular cli
```
npm install -g @angular/cli
```
Navigate to the `Viewer-app` directory 
```
cd path\to\directory\Viewer-app
```
Run the angular build
```
ng b --prod 
```
This command will ask angular to compile the application into a simple distributable single page app. It is worth noting that this distributable is uglified. The outputted files are under the folder `Viewer-app\dist\Viewer-app`

### Running the application in developer mode
After navigating a terminal session into the `Viewer-app` directory, you can run the following command to build and launch a web client.
```
ng s
```

### Note about companion app.
The app will attempt to communicate with the API on `https://localhost:44397` and therefore requires the api to first be running and available at that address.


## Built With

* [.Net Core 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1) - C# Framework of which the project is written in.
* [ASP.Net Core](https://dotnet.microsoft.com/apps/aspnet) - Web Api/MVC framework used to create web apps and apis.
* [Swashbuckle](https://github.com/domaindrivendev/Swashbuckle) - An openapi spec generator based off C# controllers and their annotations.
* [Entity Framework Core](hhttps://github.com/dotnet/efcore) - An ORM to simplify database/data-persistant storage.
* [MS Test](https://github.com/microsoft/testfx) - Microsoft's test adapter allowing the creation and running of unit tests.
* [Moq](https://github.com/moq/moq4) - A testing extention that allows the configuration of facades.
* [BCrypt](https://github.com/BcryptNet/bcrypt.net) - A passwork hashing library providing a method to securely store and verify passwords.

## Versioning

This repository uses the Git FLow principles to aid in the development lifecycle.

- `master` - Branch which releases will be taken from
- `development` - Working copy of the code, in which all testing should still pass.
- `feature/*` - Itemised units of work completed, tested and then merged into development 

## Authors

* **Ross Burchnall** - [Send me a message](mailto:r-burchnall@hotmail.co.uk)

## License

This project is licensed under the MIT License
